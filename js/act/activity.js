let index = 0
var mem_NO
var actNum

let act_Data
let ticket
let chat

let isMore_act = true
let isChattingBroad = false
let isDragImgMouseDown = true

let colorStyle = '#000000'
let lineWidth = 8
let dataURLArray = []

let X = []
let Z = []
let rY = []
let zIndex = []

//
//

//
//
const getPosition = element => {
  var x = 0
  var y = 0
  while (element) {
    x += element.offsetLeft - element.scrollLeft + element.clientLeft
    y += element.offsetTop - element.scrollLeft + element.clientTop
    element = element.offsetParent
  }

  return {
    x: x,
    y: y
  }
}

const renewCompetitor = act_Data => {
  // 頁面初始化輪播圖
  let act_carousel_All = document.querySelector('.act_carousel_All')
  if (window.innerWidth > 414) {
    act_carousel_All.innerHTML = `<img class="arrow leftArrow" src="./img/act/leftArrow.png">
    <img class="arrow rightArrow" src="./img/act/rightArrow.png">`

    let left = document.querySelector('.leftArrow')
    let right = document.querySelector('.rightArrow')
    left.addEventListener('click', function(e) {
      e.stopPropagation()
      carousel(+1)
    })
    right.addEventListener('click', function(e) {
      e.stopPropagation()
      carousel(-1)
    })
  } else {
    act_carousel_All.innerHTML = ''
  }

  for (let i = 0; i < act_Data.length; i++) {
    let div = document.createElement('div')
    div.className = 'act_competitor'
    div.style.backgroundImage = `url('${act_Data[i].src}')`
    div.style.transform = `
      translateX(${act_Data[i].x}px) 
      translateZ(${act_Data[i].z}px) 
      rotateY(${act_Data[i].ry}deg)
      `

    div.style.width = act_carousel_All.clientWidth * 0.24 + 'px'
    div.style.height = act_carousel_All.clientWidth * 0.24 * 1.44 + 'px'

    div.style.zIndex = act_Data[i].zIndex
    div.dataset.register_no = act_Data[i].registerNO

    act_carousel_All.appendChild(div)
  }

  let act_competitor = document.querySelectorAll('.act_competitor')
  for (let i = 0; i < act_competitor.length; i++) {
    let x = act_competitor[i].style.transform.split(' ')[0]
    x = +[...x].slice(11, x.length - 3).join('')
    X.push(x)
    let z = act_competitor[i].style.transform.split(' ')[1]
    z = +[...z].slice(11, z.length - 3).join('')
    Z.push(z)
    let ry = act_competitor[i].style.transform.split(' ')[2]
    ry = +[...ry].slice(8, ry.length - 4).join('')
    rY.push(ry)
    let z_index = +act_competitor[i].style.zIndex
    zIndex.push(z_index)
  }
}

const chatting = chatArr => {
  // console.log(chatArr)
  // 留言動畫
  let len = +chatArr.length
  let i = len - 1
  let str = '<span class="outerSpan">'
  while (i >= 0) {
    str += `<span>${chatArr[i][1]}</span>`
    i--
  }
  str += '</span>'
  return str
}

const carousel = change => {
  // 輪播
  let ticketNo = document.querySelector('.ticketNo')
  let act_competitor = document.querySelectorAll('.act_competitor')

  index = (index + change + act_competitor.length) % act_competitor.length
  // console.log(index);
  for (let i = 0; i < act_competitor.length; i++) {
    act_competitor[i].style.transform = `
      translateZ(${Z[(index + i) % act_competitor.length]}px)
      translateX(${X[(index + i) % act_competitor.length]}px) 
      rotateY(${rY[(index + i) % act_competitor.length]}deg)
      `

    act_competitor[i].style.zIndex = zIndex[(index + i) % act_competitor.length]

    if (window.innerWidth > 768) {
      act_competitor[i].style.zIndex === Math.max(...zIndex).toString()
        ? setTimeout(function() {
            act_competitor[i].addEventListener('click', showChattingBoard)
          }, 500)
        : act_competitor[i].removeEventListener('click', showChattingBoard)
    }
    if (window.innerWidth <= 768) {
      act_competitor[i].style.zIndex === '1'
        ? (act_competitor[i].style.opacity = '0')
        : (act_competitor[i].style.opacity = '1')
    }

    if (!isChattingBroad) {
      act_competitor[i].style.zIndex === Math.max(...zIndex).toString()
        ? (act_competitor[i].innerHTML = chatting(
            chat[ticket.length - 1 - index]
          ))
        : (act_competitor[i].innerHTML = '')

      if (window.innerWidth > 768) {
        act_competitor[i].style.zIndex === Math.max(...zIndex).toString()
          ? act_competitor[i].classList.add('activeHover')
          : act_competitor[i].classList.remove('activeHover')
      }
    }

    act_competitor[i].style.zIndex === Math.max(...zIndex).toString()
      ? act_competitor[i].classList.remove('active')
      : act_competitor[i].classList.add('active')

    act_competitor[i].style.zIndex === Math.max(...zIndex).toString()
      ? (act_competitor[i].style.cursor = 'pointer')
      : (act_competitor[i].style.cursor = 'grab')
  }
  ticketNo.innerText = ticket[ticket.length - 1 - index]
  chatBoardCarousel()
}

// 觸控輪播
let act_touchXstart, act_touchXend
const touchStartCarousel = e => {
  e.stopPropagation()
  e.preventDefault()

  e.touches ? (act_touchXstart = e.touches[0].pageX) : ''
  let act_carousel_All = document.querySelector('.act_carousel_All')
  act_carousel_All.addEventListener('touchmove', e => {
    act_touchXend = e.touches[0].pageX
  })
}

// 滑鼠拖曳輪播
let act_mouseXstart, act_mouseXend
const mouseStartCarousel = e => {
  e.stopPropagation()
  e.preventDefault()
  let act_competitor = document.querySelectorAll('.act_competitor')
  act_competitor.forEach(item => (item.style.cursor = 'grabbing'))

  e.touches ? '' : (act_mouseXstart = e.pageX)
  let act_carousel_All = document.querySelector('.act_carousel_All')
  act_carousel_All.addEventListener('mousemove', e => {
    act_mouseXend = e.pageX
  })
}

// 更多活動頁面跳窗動畫
// 遮罩的開關 及 關閉按鈕 點擊事件
const act_moreAct = () => {
  // 更多活動頁面跳窗動畫
  let act_mask = document.querySelector('.act_mask')
  act_mask.style.display = 'block'

  let tweenmoreActEffect = act_moreActEffect()

  act_mask.addEventListener('click', () => {
    // 移除跳窗
    act_mask.style.display = 'none'
    act_ActivityContainer.style.display = 'none'
    // 使每次跳窗時都是在更多活動的頁面
    changeActSection(false)
    isMore_act = true
    // 讓更多活動頁的跳窗動畫每次都重新來過
    tweenmoreActEffect.pause()
    // 更多活動頁面 及 報名頁面 初始化狀態
    let act_ActivityContainerTopBar = document.querySelector(
      '.act_ActivityContainerTopBar'
    )
    act_ActivityContainerTopBar.style.display = 'block'
    let act_signUpActivity = document.querySelector('.act_signUpActivity')
    act_signUpActivity.style.display = 'none'
    // 離開時清掉畫布上的東西
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
    let act_dragImg = document.querySelector('.act_dragImg')
    act_dragImg.style.display = 'none'
    let act_imgAdvice = document.querySelector('.act_imgAdvice')
    act_imgAdvice.children[0].innerText = ''
    // 恢復可點及狀態
    let switchBtn = document.querySelector('.switchBtn')
    switchBtn.style.pointerEvents = 'auto'
    // 清掉發起活動
    let act_showPic = document.querySelector('.act_showPic')
    act_showPic.innerHTML = ''
    let act_refImgName = document.querySelector('.act_refImgName')
    act_refImgName.innerText = ''
    //回復關閉紐
    TweenMax.to('.closeBtn', 0.3, {
      opacity: 1,
      autoAlpha: 1
    })
  })

  let closeBtn = document.querySelector('.closeBtn')
  closeBtn.addEventListener('click', () => {
    // 移除跳窗
    act_mask.style.display = 'none'
    act_ActivityContainer.style.display = 'none'
    // 使每次跳窗時都是在更多活動的頁面
    changeActSection(false)
    isMore_act = true
    // 讓更多活動頁的跳窗動畫每次都重新來過
    tweenmoreActEffect.pause()
    // 更多活動頁面 及 報名頁面 初始化狀態
    let act_ActivityContainerTopBar = document.querySelector(
      '.act_ActivityContainerTopBar'
    )
    act_ActivityContainerTopBar.style.display = 'block'
    let act_signUpActivity = document.querySelector('.act_signUpActivity')
    act_signUpActivity.style.display = 'none'
    // 離開時清掉畫布上的東西
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
    let act_dragImg = document.querySelector('.act_dragImg')
    act_dragImg.style.display = 'none'
    let act_imgAdvice = document.querySelector('.act_imgAdvice')
    act_imgAdvice.children[0].innerText = ''
    // 恢復可點及狀態
    let switchBtn = document.querySelector('.switchBtn')
    switchBtn.style.pointerEvents = 'auto'
    // 清掉發起活動
    let act_showPic = document.querySelector('.act_showPic')
    act_showPic.innerHTML = ''
    let act_refImgName = document.querySelector('.act_refImgName')
    act_refImgName.innerText = ''
    TweenMax.to('.closeBtn', 0.3, {
      opacity: 1,
      autoAlpha: 1
    })
  })
}

// 顯示更多活動跳窗動畫
const act_moreActEffect = () => {
  let tween = new TimelineMax()
  tween
    .to('#act_ActivityContainer', 0.1, {
      scale: 0.01,
      onComplete: function() {
        let act_ActivityContainer = document.querySelector(
          '#act_ActivityContainer'
        )
        act_ActivityContainer.style.display = 'block'
      }
    })
    .to('#act_ActivityContainer', 0.5, {
      scale: 1,
      ease: ExpoScaleEase.config(1, 20)
    })

  return tween
}

// 切換 更多活動 及 發起活動 按鈕旁的箭頭動畫
const drawArrowSVG = () => {
  // 切換 更多活動 及 發起活動 按鈕旁的箭頭動畫
  let tweenArrow = new TimelineMax()
    .to('.arrowSVG', 0, {
      rotationZ: 45,
      rotationX: 180
    })
    .to(['.draw-arrow', '.tail-1', '.tail-2'], 0, {
      strokeDashoffset: 400
    })
    .to('.draw-arrow', 2, {
      strokeDashoffset: 0,
      delay: 0.5
    })
    .to('.tail-1', 2, {
      strokeDashoffset: 0,
      delay: -1.8
    })
    .to('.tail-2', 2, {
      strokeDashoffset: 0,
      delay: -1.9
    })

  return tweenArrow
}

// 切換 更多活動跳窗 及 發起活動跳窗
const changeActSection = isMore_act => {
  //  切換 更多活動跳窗 及 發起活動跳窗
  let act_moreActivity = document.querySelector('.act_moreActivity')
  let act_establishActivity = document.querySelector('.act_establishActivity')
  let act_ActivityContainer = document.querySelector('#act_ActivityContainer')
  let switchBtn = document.querySelector('.switchBtn')
  let tweenArrow = drawArrowSVG()

  if (isMore_act) {
    switchBtn.innerText = '更多活動'
    switchBtn.previousSibling.innerText = '發起活動'
    switchBtn.previousSibling.dataset.text = '發起活動'

    act_ActivityContainer.style.width = '50%'
    act_ActivityContainer.style.height = '75vh'
    act_ActivityContainer.style.left = '25%'
    act_ActivityContainer.style.bottom = '100px'

    act_moreActivity.style.display = 'none'
    act_establishActivity.style.display = 'block'

    new TimelineMax({
      onStart: tweenArrow.restart(true, false)
    })
      .to('.act_establishActivity', 0, {
        opacity: 0
      })
      .to('.act_establishActivity', 0.5, {
        opacity: 1,
        delay: 0.4,
        ease: Power4.ease
      })
    // 清掉發起活動
    let act_showPic = document.querySelector('.act_showPic')
    act_showPic.innerHTML = ''
    let act_refImgName = document.querySelector('.act_refImgName')
    act_refImgName.innerText = ''
  } else {
    switchBtn.innerText = '發起活動'
    switchBtn.previousSibling.innerText = '更多活動'
    switchBtn.previousSibling.dataset.text = '更多活動'

    act_ActivityContainer.style.width = '70%'
    act_ActivityContainer.style.height = '85vh'
    act_ActivityContainer.style.left = '15%'
    act_ActivityContainer.style.bottom = '50px'

    // act_moreActivity.style.display = "block";
    act_establishActivity.style.display = 'none'

    new TimelineMax({
      onStart: tweenArrow.restart(true, false)
    })
      .to('.act_moreActivity', 0, {
        opacity: 0
      })
      .to('.act_moreActivity', 0.5, {
        opacity: 1,
        delay: 0.4,
        ease: Power4.ease,
        onStart: function() {
          act_moreActivity.style.display = 'block'
        }
      })
  }
}

// 點選報名的活動資訊 轉到 報名活動頁面
const showSignUpInfo = (title, date, content, actNum) => {
  // 點選報名的活動資訊 轉到 報名活動頁面
  let act_signUpTitle = document.querySelector('.act_signUpTitle')
  let act_signUpDate = document.querySelector('.act_signUpDate')
  let act_signUpContent = document.querySelector('.act_signUpContent')
  let act_signUpSubmitBtn = document.querySelector('.act_signUpSubmitBtn')
  act_signUpSubmitBtn.dataset.actnum = actNum

  act_signUpTitle.childNodes[1].innerText = title
  act_signUpDate.childNodes[1].innerText = date
  act_signUpContent.childNodes[2].innerText = content
}

// 更多活動 到 報名活動
// 清除畫布按鈕
const showSignUpPanel = e => {
  // 人數滿了不能被點擊
  // 已報名人數
  let a = +e.target.previousElementSibling.previousElementSibling.innerText
  // 人數上限
  let b = +e.target.previousElementSibling.innerText
  if (a === b) {
    Swal.fire({
      html: '<div style="font-size:18px">報名人數已滿囉!!!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }
  // 讓更多活動及發起活動切換按鈕不能被點擊
  let switchBtn = document.querySelector('.switchBtn')
  switchBtn.style.pointerEvents = 'none'

  // 資料送到報名頁面
  let activity = e.target.parentNode.parentNode
  let actNum = e.target.parentNode.children[0].dataset.actnum
  let title = activity.children[0].innerText
  let date = activity.children[1].innerText
  let content = activity.children[2].innerText
  showSignUpInfo(title, date, content, actNum)

  // 更多活動 到 報名活動
  // console.log("showSignUpPanel");
  let act_ActivityContainerTopBar = document.querySelector(
    '.act_ActivityContainerTopBar'
  )
  let act_moreActivity = document.querySelector('.act_moreActivity')
  let act_signUpActivity = document.querySelector('.act_signUpActivity')
  let act_signUpTopBar = document.querySelector('.act_signUpTopBar')
  TweenMax.to('.closeBtn', 0.3, {
    opacity: 0,
    autoAlpha: 0
  })

  let tween = new TimelineMax()
  tween
    .to('.act_moreActivity', 0.5, {
      y: 50,
      opacity: 0,
      onComplete: function() {
        act_moreActivity.style.display = 'none'
      }
    })
    .to('.act_ActivityContainerTopBar', 0.5, {
      y: 50,
      opacity: 0,
      delay: -0.2,
      onComplete: function() {
        act_ActivityContainerTopBar.style.display = 'none'
        act_signUpActivity.style.display = 'block'
        act_signUpTopBar.childNodes[0].style.opacity = '0'
      }
    })
    .fromTo(
      '.act_signUpWrap',
      1,
      {
        x: 50,
        opacity: 0,
        onComplete: function() {
          let img = act_signUpTopBar.childNodes[0]
          TweenMax.fromTo(
            img,
            0.5,
            {
              x: 50,
              opacity: 0
            },
            {
              x: 0,
              opacity: 1
            }
          )
          TweenMax.to('.act_ActivityContainerTopBar', 0, {
            y: 0,
            opacity: 1
          })
          TweenMax.to('.act_moreActivity', 0, {
            y: 0,
            opacity: 1
          })
        }
      },
      {
        x: 0,
        opacity: 1,
        delay: 0.5
      }
    )

  // 清除畫布
  let clearAll = document.querySelector('.clearAll')
  clearAll.addEventListener('click', e => {
    let act_dragImg = document.querySelector('.act_dragImg')
    act_dragImg.style.left = '0px'
    act_dragImg.style.top = '0px'
    act_dragImg.style.display = 'none'
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
    ctx.fillStyle = '#E8E8E8'
    ctx.fillRect(0, 0, myCanvas.width, myCanvas.height)
    dataURLArray = []
  })

  brushChosenColor()
  brushChosenSize()

  // 回更多活動頁面
  let goBackBtn = act_signUpTopBar.childNodes[0]
  goBackBtn.addEventListener('click', goBack_moreAct)
}

// 報名活動 返回 更多活動
const goBack_moreAct = () => {
  // console.log("go back");
  // 報名活動 返回 更多活動
  let switchBtn = document.querySelector('.switchBtn')
  switchBtn.style.pointerEvents = 'auto'

  let act_ActivityContainerTopBar = document.querySelector(
    '.act_ActivityContainerTopBar'
  )
  let act_moreActivity = document.querySelector('.act_moreActivity')
  let act_signUpActivity = document.querySelector('.act_signUpActivity')
  let act_signUpTopBar = document.querySelector('.act_signUpTopBar')

  TweenMax.to('.closeBtn', 0.3, {
    opacity: 1,
    autoAlpha: 1
  })

  goBack_moreAct_tween = new TimelineMax()
  goBack_moreAct_tween
    .to('.act_signUpWrap', 0.5, {
      x: -50,
      opacity: 0
    })
    .to('.act_signUpTopBar', 0.5, {
      x: -50,
      opacity: 0,
      onComplete: function() {
        act_signUpActivity.style.display = 'none'
        act_ActivityContainerTopBar.style.display = 'block'
      }
    })
    .fromTo(
      '.act_ActivityContainerTopBar',
      0.5,
      {
        y: 50,
        opacity: 0,
        onComplete: function() {
          act_moreActivity.style.display = 'block'
          TweenMax.to('.act_signUpTopBar', 0, {
            x: 0,
            opacity: 1
          })
          act_signUpTopBar.childNodes[0].style.opacity = '1'
        }
      },
      {
        y: 0,
        opacity: 1
      }
    )
    .fromTo(
      '.act_moreActivity',
      0.5,
      {
        y: 50,
        opacity: 0
      },
      {
        y: 0,
        opacity: 1,
        delay: -0.2
      }
    )

  // 離開時清掉畫布上的東西
  ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
  let act_dragImg = document.querySelector('.act_dragImg')
  act_dragImg.src = ''
  act_dragImg.style.display = 'none'
  let act_imgAdvice = document.querySelector('.act_imgAdvice')
  act_imgAdvice.children[0].innerText = ''
}

// 發起活動上傳照片
const uploadActRefImg = e => {
  if (e.target.files[0]) {
    let act_showPic = document.querySelector('.act_showPic')
    let act_refImgName = document.querySelector('.act_refImgName')
    act_refImgName.innerText = e.target.files[0].name

    let reader = new FileReader()
    reader.readAsDataURL(e.target.files[0])
    reader.onload = function(e) {
      act_showPic.innerHTML = `<img src="${e.target.result}">`
    }
  }
}
// 發起活動表單送出
const establishActForm = e => {
  e.preventDefault()
  // 確認是否登入
  if (mem_NO === -1) {
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  let act_titleInput = document.querySelector('.act_titleInput')
  let act_MaxLimitSelection = document.querySelector('.act_MaxLimitSelection')
  let act_myTextarea = document.querySelector('.act_myTextarea')
  let act_showPic = document.querySelector('.act_showPic')
  if (!act_showPic.hasChildNodes()) {
    Swal.fire({
      html: '<div style="font-size:18px">請上傳照片!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }
  if (!act_titleInput.value) {
    Swal.fire({
      html: '<div style="font-size:18px">請填寫標題!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }
  if (!act_MaxLimitSelection.value) {
    Swal.fire({
      html: '<div style="font-size:18px">請填寫人數!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }
  if (!act_myTextarea.value) {
    Swal.fire({
      html: '<div style="font-size:18px">請填寫敘述!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  // 送出表單
  Swal.fire({
    html: '<div style="font-size:18px">確認送出!?</div>',
    showCancelButton: true,
    confirmButtonColor: '#f46f65',
    cancelButtonColor: '#aaa',
    confirmButtonText: '確定',
    cancelButtonText: '取消',
    background: '#fff url("./img/p0080_m.jpg") center'
  }).then(result => {
    if (result.value) {
      let act_establishActWrap = document.querySelector('.act_establishActWrap')
      let myFormData = new FormData(act_establishActWrap)
      myFormData.append('mem_NO', mem_NO)
      let xhr = new XMLHttpRequest()
      xhr.open('post', './php/act/establish_act.php', true)
      xhr.send(myFormData)
      let response
      xhr.onload = function() {
        response = xhr.responseText
        // console.log(response)
        if (response === '上傳成功') {
          Swal.fire({
            html: '<div style="font-size:18px">上傳成功</div>',
            confirmButtonText: '確定',
            confirmButtonColor: '#f46f65',
            background: '#fff url("./img/p0080_m.jpg") center'
          }).then(() => {
            window.location.reload()
          })
        } else {
          Swal.fire({
            html: `<div style="font-size:18px">${response}</div>`,
            confirmButtonText: '確定',
            confirmButtonColor: '#f46f65',
            background: '#fff url("./img/p0080_m.jpg") center'
          })
        }
      }
    }
  })
}

// 報名活動上傳照片
let draggableImg
const uploadSignUpImg = e => {
  if (e.target.files[0]) {
    // 每次上傳先清空本來的東西
    dataURLArray = []
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
    ctx.fillStyle = '#E8E8E8'
    ctx.fillRect(0, 0, myCanvas.width, myCanvas.height)

    let act_dragImg = document.querySelector('.act_dragImg')
    let reader = new FileReader()
    let act_imgAdvice = document.querySelector('.act_imgAdvice')
    act_imgAdvice.children[0].innerText = e.target.files[0].name

    reader.readAsDataURL(e.target.files[0])
    reader.onload = function(e) {
      let img = new Image()
      img.src = e.target.result
      img.onload = function() {
        // 取得寬高
        draggableImg = img
        if (img.width > myCanvas.width || img.height > myCanvas.height) {
          Swal.fire({
            title: `建議使用 ${myCanvas.width} x ${myCanvas.height} 像素以下的照片!?`,
            confirmButtonText: '確認'
          })
        } else {
          act_dragImg.style.width = img.width + 'px'
          act_dragImg.style.height = img.height + 'px'
          act_dragImg.style.display = 'block'
          act_dragImg.style.left = '0px'
          act_dragImg.style.top = '0px'
          ctx.drawImage(draggableImg, 0, 0)
          dataURLArray.push(myCanvas.toDataURL())
        }
      }

      // 讓圖片可以拖移
      if (window.innerWidth > 768) {
        act_dragImg.addEventListener('mousedown', canvasDragImg)
      }
    }
  }
  document.getElementsByName('act_memSignUpImg')[0].type = 'text'
  document.getElementsByName('act_memSignUpImg')[0].type = 'file'
}
// 報名活動表單送出
const signUpActForm = e => {
  e.preventDefault()
  // 確認是否登入
  if (mem_NO === -1) {
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  if (dataURLArray.length === 0) {
    Swal.fire({
      html: '<div style="font-size:18px">請上傳照片!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  } else {
    // 送出表單
    Swal.fire({
      html: '<div style="font-size:18px">確認報名!?</div>',
      showCancelButton: true,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        let myCanvasURL = myCanvas.toDataURL()
        let form = new FormData()
        let act_imgAdvice = document.querySelector('.act_imgAdvice')
        form.append('uploadSignUpImg', act_imgAdvice.children[0].innerText)
        form.append('myCanvasURL', myCanvasURL)
        form.append('activityNum', e.target.dataset.actnum)
        form.append('mem_NO', mem_NO)
        let xhr = new XMLHttpRequest()
        xhr.open('post', './php/act/register_act.php', true)
        xhr.send(form)
        xhr.onload = () => {
          // console.log(xhr.responseText)
          if (xhr.responseText === 'complete') {
            Swal.fire({
              html: '<div style="font-size:18px">報名完成</div>',
              confirmButtonText: '確定',
              confirmButtonColor: '#f46f65',
              background: '#fff url("./img/p0080_m.jpg") center'
            }).then(() => {
              window.location.reload()
            })
          } else if (xhr.responseText === 'already') {
            Swal.fire({
              html: '<div style="font-size:18px">您已經報名該活動</div>',
              confirmButtonText: '確定',
              confirmButtonColor: '#f46f65',
              background: '#fff url("./img/p0080_m.jpg") center'
            })
          } else {
            Swal.fire({
              html: '<div style="font-size:18px">報名失敗</div>',
              confirmButtonText: '確定',
              confirmButtonColor: '#f46f65',
              background: '#fff url("./img/p0080_m.jpg") center'
            })
          }
        }
      }
    })
    // else
  }
}

//
//
//
//
// 顯示留言板
const showChattingBoard = () => {
  let act_messageBoardMask = document.querySelector('.act_messageBoardMask')
  act_messageBoardMask.style.display = 'block'

  let act_competitor = document.querySelectorAll('.act_competitor')
  act_competitor.forEach(item => {
    item.style.pointerEvents = 'none'
  })

  let act_carousel_All = document.querySelector('.act_carousel_All')
  act_carousel_All.style.zIndex = '190'

  let showChattingBoard_tween = new TimelineMax()
  showChattingBoard_tween
    .to('.act_competitor', 0.7, {
      rotationY: 0,
      x: X[3],
      z: 0,
      ease: SlowMo.ease,
      delay: -0.5,
      onComplete: function() {
        let outerSpan = document.querySelector('.outerSpan')
        outerSpan.style.display = 'none'
      }
    })
    .to('.act_competitor', 0, {
      delay: 0.5,
      onStart: function() {
        act_competitor.forEach(item => {
          item.style.boxShadow = '0px 0px 0px transparent'
          item.classList.remove('active')
          item.classList.remove('activeHover')
        })
      }
    })
    .to('.act_carousel_All', 0.5, {
      y: -360,
      onComplete: function() {
        isChattingBroad = true
        carousel(0)
      }
    })
    .fromTo(
      '.act_chattingBoard',
      0.5,
      {
        autoAlpha: 0,
        display: 'block',
        y: -300
      },
      {
        autoAlpha: 1,
        y: 0,
        delay: 0
      }
    )

  window.addEventListener('keypress', showCommentByEnter)
}
// 留言板恢復原樣
const recoverChattingBoard = () => {
  let act_carousel_All = document.querySelector('.act_carousel_All')
  act_carousel_All.style.zIndex = '100'

  if (window.innerWidth > 768) {
    let act_competitor = document.querySelectorAll('.act_competitor')
    act_competitor.forEach(item => {
      item.style.pointerEvents = 'auto'
      item.style.boxShadow = '0px 0px 10px rgba(0, 0, 0, 0.7)'
      item.classList.add('active')
      item.classList.add('activeHover')
    })
    let outerSpan = document.querySelector('.outerSpan')
    outerSpan.style.display = 'block'

    let act_messageBoardMask = document.querySelector('.act_messageBoardMask')
    act_messageBoardMask.style.display = 'none'

    let act_chattingBoard = document.querySelector('.act_chattingBoard')
    act_chattingBoard.style.display = 'none'

    isChattingBroad = false
    carousel(0)

    TweenMax.to('.act_carousel_All', 0.5, {
      y: 0
    })
  }

  window.removeEventListener('keypress', showCommentByEnter)
}

// 產生留言的模板
const commentsTemplate = comment => {
  let divCommentMessage = document.createElement('div')
  divCommentMessage.className = 'act_commentMessage'

  let divHeadImg = document.createElement('div')
  divHeadImg.className = 'act_headImg'
  divHeadImg.innerHTML = `<img src="./${comment[2]}">`
  divCommentMessage.appendChild(divHeadImg)

  let divMemberComment = document.createElement('div')
  divMemberComment.className = 'act_memberComment'
  divMemberComment.innerText = comment[1]
  divCommentMessage.appendChild(divMemberComment)

  let span = document.createElement('span')
  span.className = 'act_reportTheComment'
  span.innerHTML = '檢舉'
  span.dataset.comment_no = comment[0]
  span.addEventListener('click', reportComment)

  // 中文才有效
  if (comment.length <= 15) {
    span.style.bottom = '-10px'
  } else {
    span.style.bottom = '-25px'
  }
  divCommentMessage.appendChild(span)

  let act_commentSection = document.querySelector('.act_commentSection')
  let scrollToBottom = document.querySelector('#scrollToBottom')
  act_commentSection.insertBefore(divCommentMessage, scrollToBottom)

  let tween = new TimelineMax()
  tween
    .to(act_commentSection, 0.2, {
      scrollTo: scrollToBottom
    })
    .fromTo(
      divMemberComment,
      0.5,
      {
        y: 150,
        scale: 0
      },
      {
        y: 0,
        scale: 1
      }
    )
    .fromTo(
      span,
      0.2,
      {
        opacity: 0
      },
      {
        opacity: 1
      }
    )
}

// 會員留言 (click)
const showComment = () => {
  // 確認是否登入
  if (mem_NO === -1) {
    document.querySelector('.act_typeComments').value = ''
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  // 會員留言 (click)
  let act_typeCommentsValue = document.querySelector('.act_typeComments').value
  if (act_typeCommentsValue === '') return

  let act_competitor = document.querySelectorAll('.act_competitor')
  let register_no = ''
  act_competitor.forEach(item => {
    register_no =
      item.style.zIndex == Math.max(...zIndex)
        ? item.dataset.register_no
        : register_no
  })

  let xhr = new XMLHttpRequest()
  xhr.open('POST', './php/act/comment_act.php', true)
  xhr.setRequestHeader('Content-type', 'application/json')
  let commentDate = {
    comment_no: -1,
    mem_NO: mem_NO,
    register_no: register_no,
    comment_content: act_typeCommentsValue
  }
  xhr.send(JSON.stringify(commentDate))
  xhr.onload = () => {
    let a = JSON.parse(xhr.responseText)
    commentsTemplate([a[0], act_typeCommentsValue, a[1]])
    chat[ticket.length - 1 - index].push([a[0], act_typeCommentsValue, a[1]])
  }

  document.querySelector('.act_typeComments').value = ''
}

// 會員留言 (enter)
const showCommentByEnter = e => {
  // 會員留言 (enter)
  if (e.keyCode != 13) return

  // 確認是否登入
  if (mem_NO === -1) {
    document.querySelector('.act_typeComments').value = ''
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  let act_typeCommentsValue = document.querySelector('.act_typeComments').value
  if (act_typeCommentsValue === '') return

  let act_competitor = document.querySelectorAll('.act_competitor')
  let register_no = ''
  act_competitor.forEach(item => {
    register_no =
      item.style.zIndex == Math.max(...zIndex)
        ? item.dataset.register_no
        : register_no
  })

  let xhr = new XMLHttpRequest()
  xhr.open('POST', './php/act/comment_act.php', true)
  xhr.setRequestHeader('Content-type', 'application/json')
  let commentDate = {
    comment_no: -1,
    mem_NO: mem_NO,
    register_no: register_no,
    comment_content: act_typeCommentsValue
  }
  xhr.send(JSON.stringify(commentDate))

  xhr.onload = () => {
    let a = JSON.parse(xhr.responseText)
    commentsTemplate([a[0], act_typeCommentsValue, a[1]])
    chat[ticket.length - 1 - index].push([a[0], act_typeCommentsValue, a[1]])
  }

  document.querySelector('.act_typeComments').value = ''
}

// 檢舉留言
const reportComment = e => {
  // 確認是否登入
  if (mem_NO === -1) {
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  Swal.fire({
    html: '<div style="font-size:18px">是否要檢舉該不當留言!?</div>',
    showCancelButton: true,
    confirmButtonColor: '#f46f65',
    cancelButtonColor: '#aaa',
    confirmButtonText: '確定',
    cancelButtonText: '取消',
    background: '#fff url("./img/p0080_m.jpg") center'
  }).then(result => {
    if (result.value) {
      let xhr = new XMLHttpRequest()
      xhr.open('POST', './php/act/comment_act.php', true)
      xhr.setRequestHeader('Content-type', 'application/json')
      xhr.send(JSON.stringify({ comment_no: e.target.dataset.comment_no }))
      xhr.onload = () => {
        Swal.fire({
          html: `<div style="font-size:18px;">留言編號${xhr.responseText}已送出審核，謝謝您的提醒!!`,
          confirmButtonText: '確定',
          confirmButtonColor: '#f46f65'
        })
      }
    }
  })
}

// 輪播切換留言板內容
const chatBoardCarousel = () => {
  let act_commentSection = document.querySelector('.act_commentSection')
  act_commentSection.innerHTML = '<div id="scrollToBottom"></div>'

  let tween = new TimelineMax()
  tween
  TweenMax.fromTo(
    '.act_commentSection',
    0.3,
    {
      opacity: 0
    },
    {
      opacity: 1
    }
  )

  let eachComment = chat[ticket.length - 1 - index]
  eachComment.forEach(item => commentsTemplate(item))
}

//
//
//
//
//
// 畫布
const drawStart = e => {
  if (dataURLArray.length != 0) {
    let startX = e.pageX - getPosition(document.querySelector('#myCanvas')).x
    let startY = e.pageY - getPosition(document.querySelector('#myCanvas')).y

    ctx.beginPath()
    ctx.moveTo(startX, startY)
    // "butt" || "round" || "square";
    ctx.lineCap = 'round'
    // "bevel" || "round" || "miter"
    ctx.lineJoin = 'round'
    ctx.lineWidth = lineWidth
    ctx.strokeStyle = colorStyle
    myCanvas.addEventListener('mousemove', draw)
    myCanvas.addEventListener('mouseup', endDraw)
  } else {
    Swal.fire({
      html: '<div style="font-size:18px">請先上傳照片!?</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
  }
}

const draw = e => {
  // 扣掉 canvas 到 父層左上角的距離
  let drawX = e.pageX - getPosition(document.querySelector('#myCanvas')).x
  let drawY = e.pageY - getPosition(document.querySelector('#myCanvas')).y

  ctx.lineTo(drawX, drawY)
  ctx.stroke()
}

const endDraw = e => {
  e.stopPropagation()
  myCanvas.removeEventListener('mousemove', draw)
  myCanvas.removeEventListener('mousemove', draw)
  myCanvas.removeEventListener('mouseup', endDraw)
}

const brushMode = e => {
  let brushCanvas = document.getElementById('brushCanvas')

  brushCanvas.style.width = lineWidth + 'px'
  brushCanvas.style.height = lineWidth + 'px'
  brushCanvas.style.backgroundColor = colorStyle

  brushCanvas.style.display = 'block'
  brushCanvas.style.pointerEvents = 'none'
  brushCanvas.style.left =
    e.pageX - getPosition(document.querySelector('#myCanvas')).x + 'px'
  brushCanvas.style.top =
    e.pageY - getPosition(document.querySelector('#myCanvas')).y + 'px'
  brushCanvas.style.transform = 'translate(-50%, -50%)'
  // myCanvas.style.cursor = "none";

  myCanvas.addEventListener('mouseleave', function() {
    brushCanvas.style.display = 'none'
  })
}

const brushChosenSize = () => {
  // 選擇大小
  let act_size = document.querySelectorAll('.act_size')
  act_size.forEach(item => {
    item.addEventListener('click', e => {
      for (let i = 0; i < act_size.length; i++) {
        act_size[i].hasChildNodes()
          ? act_size[i].removeChild(act_size[i].lastChild)
          : ''
      }
      lineWidth = e.target.dataset.size
      e.target.innerHTML = '<span class="activeSpan"></span>'
    })
  })
}

const brushChosenColor = () => {
  // 選擇顏色
  document.querySelector('.jscolor').addEventListener('change', () => {
    colorStyle = '#' + document.querySelector('.jscolor').value
  })
}

const canvasDragImg = e => {
  let act_dragImg = document.querySelector('.act_dragImg')
  act_dragImg.style.pointerEvents = 'auto'
  isDragImgMouseDown = true

  let act_canvas = document.querySelector('.act_canvas')

  let x = e.clientX - act_dragImg.offsetLeft
  let y = e.clientY - act_dragImg.offsetTop

  e.target.addEventListener('mousemove', e => {
    let moveX
    let moveY
    // 限制範圍 X
    e.clientX - x > act_canvas.clientWidth - act_dragImg.clientWidth
      ? (moveX = act_canvas.clientWidth - act_dragImg.clientWidth)
      : e.clientX - x < 0
      ? (moveX = 0)
      : (moveX = e.clientX - x)
    // 限制範圍 Y
    e.clientY - y > act_canvas.clientHeight - act_dragImg.clientHeight
      ? (moveY = act_canvas.clientHeight - act_dragImg.clientHeight)
      : e.clientY - y < 0
      ? (moveY = 0)
      : (moveY = e.clientY - y)

    if (isDragImgMouseDown) {
      // 調整位置
      e.target.style.left = moveX + 'px'
      e.target.style.top = moveY + 'px'
      // 每次上傳先清空本來的東西
      ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
      ctx.fillStyle = '#E8E8E8'
      ctx.fillRect(0, 0, myCanvas.width, myCanvas.height)
      ctx.drawImage(draggableImg, moveX, moveY)
    }
  })

  e.target.addEventListener('mouseup', e => {
    isDragImgMouseDown = false
  })
}

// 投票按鈕送出
// 活動更新後要更新按鈕的data屬性
const voting = e => {
  // 確認是否登入
  if (mem_NO === -1) {
    Swal.fire({
      html: '<div style="font-size:18px">還沒登入會員嗎?? 快去登入吧!</div>',
      confirmButtonText: '確定',
      confirmButtonColor: '#f46f65',
      background: '#fff url("./img/p0080_m.jpg") center'
    })
    return
  }

  let act_competitor = document.querySelectorAll('.act_competitor')
  let register_no = ''
  act_competitor.forEach(item => {
    register_no =
      item.style.zIndex == Math.max(...zIndex)
        ? item.dataset.register_no
        : register_no
  })

  Swal.fire({
    html: '<div style= "font-size:18px;">每人只有一票的機會喔!!</div>',
    showCancelButton: true,
    confirmButtonColor: '#f46f65',
    cancelButtonColor: '#aaa',
    confirmButtonText: '確定',
    cancelButtonText: '取消',
    background: '#fff url("./img/p0080_m.jpg") center'
  }).then(result => {
    if (result.value) {
      let xhr = new XMLHttpRequest()
      xhr.open('post', './php/act/voteBtn_act.php', true)
      xhr.setRequestHeader('Content-type', 'application/json')
      let jsonObj = {
        act_Num: e.target.dataset.actnum,
        mem_NO: mem_NO,
        register_no: register_no
      }
      jsonObj = JSON.stringify(jsonObj)
      xhr.send(jsonObj)
      xhr.onload = () => {
        // console.log(xhr.responseText)
        if (xhr.responseText === 'voted') {
          Swal.fire({
            html: '<div style="font-size:18px">您已經投過票囉!!</div>',
            confirmButtonText: '確定',
            confirmButtonColor: '#f46f65',
            background: '#fff url("./img/p0080_m.jpg") center'
          })
        } else if (xhr.responseText === 'down') {
          ticket[ticket.length - 1 - index] =
            +ticket[ticket.length - 1 - index] + 1
          let ticketNo = document.querySelector('.ticketNo')
          ticketNo.innerHTML = ticket[ticket.length - 1 - index]
          Swal.fire({
            html: '<div style="font-size:18px">完成!!</div>',
            confirmButtonText: '確定',
            confirmButtonColor: '#f46f65',
            background: '#fff url("./img/p0080_m.jpg") center'
          })
        }
      }
    }

    //
  })
}

//
//
//
//
//
//

var robotTime = 0
function robotTalk() {
  let arr = [
    '想留言跟各位巨巨們說話？點擊照片吧！',
    '活動讓你心動了嗎？趕快成為會員吧！',
    '快快投下您神聖的一票吧!!'
  ]
  if (robotTime < arr.length) {
    document.getElementById('forRobotTalk').innerText = arr[robotTime]
    robotTime++
  } else {
    robotTime = 0
  }
}

// 偵測是否有觸控事件
function isMobile() {
  try {
    document.createEvent('TouchEvent')
    return true
  } catch (e) {
    return false
  }
}

window.addEventListener('load', e => {
  // 開啟更多活動跳窗
  let act_moreActBtn = document.querySelector('.act_moreActBtn')
  act_moreActBtn.addEventListener('click', act_moreAct)
  let act_moreActBtnbelow1400px = document.querySelector(
    '.act_moreActBtnbelow1400px'
  )
  act_moreActBtnbelow1400px.addEventListener('click', act_moreAct)

  // 切換 更多活動跳窗 及 發起活動跳窗
  let switchBtn = document.querySelector('.switchBtn')
  switchBtn.addEventListener('click', function() {
    changeActSection(isMore_act)
    isMore_act = !isMore_act
  })

  // 初始化畫布
  myCanvas = document.getElementById('myCanvas')
  ctx = myCanvas.getContext('2d')
  ctx.fillStyle = '#E8E8E8'
  ctx.fillRect(0, 0, myCanvas.width, myCanvas.height)
  if (window.innerWidth > 768) {
    let brushCanvas = document.getElementById('brushCanvas')
    brushCanvas.style.width = lineWidth + 'px'
    brushCanvas.style.height = lineWidth + 'px'

    let movePattern = document.querySelector('.movePattern')
    // 分移動模式跟畫畫模式
    movePattern.addEventListener('click', e => {
      let act_dragImg = document.querySelector('.act_dragImg')
      if (e.target.innerText === '移動') {
        e.target.innerText = '畫畫'
        act_dragImg.style.pointerEvents = 'none'
        myCanvas.addEventListener('mousedown', drawStart)
        myCanvas.addEventListener('mousemove', brushMode)
        myCanvas.style.cursor = 'none'
      } else {
        e.target.innerText = '移動'
        act_dragImg.style.pointerEvents = 'auto'
        myCanvas.removeEventListener('mousedown', drawStart)
        myCanvas.removeEventListener('mousemove', brushMode)
        myCanvas.style.cursor = 'default'
      }
    })
  }

  // 留言板輸入鍵
  let act_submitComment = document.querySelector('.act_submitComment')
  act_submitComment.addEventListener('click', showComment)

  // 留言關閉
  let act_commentCloseBtn = document.querySelector('.act_commentCloseBtn')
  act_commentCloseBtn.addEventListener('click', recoverChattingBoard)

  // 觸控事件輪播
  if (isMobile()) {
    // 輪播觸控滑動
    let act_carousel_All = document.querySelector('.act_carousel_All')
    act_carousel_All.addEventListener('touchstart', touchStartCarousel)
    act_carousel_All.addEventListener('touchend', e => {
      e.preventDefault()
      if (isNaN(act_touchXend - act_touchXstart)) return
      act_touchXend - act_touchXstart !== 0
        ? act_touchXend - act_touchXstart < 0
          ? carousel(-1)
          : carousel(+1)
        : ''
    })
  }

  // 滑鼠事件輪播
  let act_carousel_All = document.querySelector('.act_carousel_All')
  act_carousel_All.addEventListener('mousedown', mouseStartCarousel)
  act_carousel_All.addEventListener('mouseup', e => {
    e.preventDefault()
    let act_competitor = document.querySelectorAll('.act_competitor')
    act_competitor.forEach(item => (item.style.cursor = 'grab'))
    if (isNaN(act_mouseXend - act_mouseXstart)) return
    act_mouseXend - act_mouseXstart !== 0
      ? act_mouseXend - act_mouseXstart < 0
        ? carousel(-1)
        : carousel(+1)
      : ''
  })

  // 發起活動上傳按鈕
  let act_uploadActRefImg = document.querySelector('.act_uploadActRefImg')
  act_uploadActRefImg.addEventListener('change', uploadActRefImg)
  let act_establishSubmitBtn = document.querySelector('.act_establishSubmitBtn')
  act_establishSubmitBtn.addEventListener('click', establishActForm)

  // 報名活動上傳按鈕
  let act_uploadimgBtn = document.querySelector('.act_uploadimgBtn')
  act_uploadimgBtn.addEventListener('change', uploadSignUpImg)
  let act_signUpSubmitBtn = document.querySelector('.act_signUpSubmitBtn')
  act_signUpSubmitBtn.addEventListener('click', signUpActForm)

  // 投票按鈕
  let act_voteBtn = document.querySelector('.act_voteBtn')
  act_voteBtn.addEventListener('click', voting)

  //
  // 機器人
  let t3 = new TimelineMax()
  t3.to('.robotHead', 0, {
    y: -40,
    x: 0
  })
  t3.to('.robotHead', 1, {
    y: -80,
    x: 0
  })
  setInterval(robotTalk, 2000)
})

//
//
//
//
// 全部報名活動的template
const all_activity = (
  str,
  activity_Poster,
  activity_Name,
  activity_Info,
  activity_NO,
  maximum_num,
  signUp_NO,
  activity_Date
) => {
  let dd = new Date(activity_Date)
  dd.setDate(dd.getDate() + 13)

  str += `
  <div class="act_eachAct upon768px">
  <div class="imgBox upon768px front">
      <img src="./php/act/${activity_Poster}">
  </div>
  <div class="act_details back">
      <div class="act_competitionTitle">${activity_Name}</div>
      <div class="act_date">
      ${activity_Date.split('-')[1]}/${
    activity_Date.split('-')[2]
  } ~ ${dd.getMonth() + 1}/${dd.getDate()}
      </div>
      <div class="act_competitionContent">
          ${activity_Info}
      </div>
      <div class="act_application">
          <span class="act_No_application" data-actNum="${activity_NO}">${signUp_NO}</span> / <span>${maximum_num}</span>
          <span class="signUpBtn">我要報名</span>
      </div>
  </div>
</div>`

  return str
}

// 右下角toggleClass
const toggleClassFun = () => {
  let tweenToggleClassFun = new TimelineMax()
  tweenToggleClassFun
    .to('burgerList', 0, {
      onComplete: function() {
        $('.burgerList').toggleClass('bgColorToggle')
        $('.hamburger-1').toggleClass('cross-right')
        $('.hamburger-2').toggleClass('cross-hide')
        $('.hamburger-3').toggleClass('cross-left')
        $('.wobble').toggleClass('ripple')
      }
    })
    .to('.slideOut1', 0.1, {
      onStart: function() {
        $('.slideOut1').toggleClass('slide-out')
      }
    })
    .to('.slideOut2', 0.1, {
      onStart: function() {
        $('.slideOut2').toggleClass('slide-out')
      }
    })
    .to('.slideOut3', 0.1, {
      onStart: function() {
        $('.slideOut3').toggleClass('slide-out')
      }
    })
}

//
//
//
// jQuery
$(document).ready(async function() {
  //是否取得會員編號
  await $.ajax({
    url: './php/header.php',
    type: 'post',
    dataType: 'json',
    success: function(data) {
      // 未登入是 -1 登入後則是會員編號
      mem_NO = data.mem_NO ? data.mem_NO : -1
      // console.log(mem_NO)
    },
    error: function(data) {
      console.log('memberNO')
    }
  })
  //
  // 先要取得 當期活動 編號
  await $.ajax({
    url: './php/act/loadActivity.php',
    type: 'POST',
    dataType: 'json',
    data: { getActNum: 1 },
    success: function(data) {
      // console.log(data)
      actNum = data.actNum
      // 變更投票按鈕活動編號
      $('.act_voteBtn').attr('data-actnum', actNum)
    },
    error: function(data) {
      // console.log(data)
      // console.log('當期活動編號')
      alert('伺服器維修中...')
    }
  })

  //
  //
  act_Data = []
  ticket = []
  chat = []
  // 活動取資料
  // (參賽者、票數、留言資料)
  await $.ajax({
    url: './php/act/loadActCompetitor.php',
    type: 'POST',
    dataType: 'JSON',
    data: JSON.stringify({ actNum: actNum }),
    success: async function(requireData) {
      let resizeCarousel = $('.act_carousel_All').width() / 10
      // 前七筆參賽者資料
      if ($(window).width() > 768) {
        act_Data.push({
          registerNO: requireData[0].registration_NO,
          src: `./php/act/${requireData[0].registration_Pic}`,
          x: resizeCarousel * 0,
          z: 0,
          zIndex: 2,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[1].registration_NO,
          src: `./php/act/${requireData[1].registration_Pic}`,
          x: resizeCarousel * 1,
          z: 50,
          zIndex: 3,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[2].registration_NO,
          src: `./php/act/${requireData[2].registration_Pic}`,
          x: resizeCarousel * 2,
          z: 100,
          zIndex: 4,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[3].registration_NO,
          src: `./php/act/${requireData[3].registration_Pic}`,
          x: resizeCarousel * 4,
          z: 200,
          zIndex: 5,
          ry: 0
        })
        act_Data.push({
          registerNO: requireData[4].registration_NO,
          src: `./php/act/${requireData[4].registration_Pic}`,
          x: resizeCarousel * 6,
          z: 100,
          zIndex: 4,
          ry: -45
        })
        act_Data.push({
          registerNO: requireData[5].registration_NO,
          src: `./php/act/${requireData[5].registration_Pic}`,
          x: resizeCarousel * 7,
          z: 50,
          zIndex: 3,
          ry: -45
        })
        act_Data.push({
          registerNO: requireData[6].registration_NO,
          src: `./php/act/${requireData[6].registration_Pic}`,
          x: resizeCarousel * 8,
          z: 0,
          zIndex: 2,
          ry: -45
        })
      } else {
        act_Data.push({
          registerNO: requireData[0].registration_NO,
          src: `./php/act/${requireData[0].registration_Pic}`,
          x: -50,
          z: 0,
          zIndex: 2,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[1].registration_NO,
          src: `./php/act/${requireData[1].registration_Pic}`,
          x: -50,
          z: 50,
          zIndex: 3,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[2].registration_NO,
          src: `./php/act/${requireData[2].registration_Pic}`,
          x: 100,
          z: 100,
          zIndex: 4,
          ry: 45
        })
        act_Data.push({
          registerNO: requireData[3].registration_NO,
          src: `./php/act/${requireData[3].registration_Pic}`,
          x: 300,
          z: 250,
          zIndex: 5,
          ry: 0
        })
        act_Data.push({
          registerNO: requireData[4].registration_NO,
          src: `./php/act/${requireData[4].registration_Pic}`,
          x: 500,
          z: 100,
          zIndex: 4,
          ry: -45
        })
        act_Data.push({
          registerNO: requireData[5].registration_NO,
          src: `./php/act/${requireData[5].registration_Pic}`,
          x: 650,
          z: 50,
          zIndex: 3,
          ry: -45
        })
        act_Data.push({
          registerNO: requireData[6].registration_NO,
          src: `./php/act/${requireData[6].registration_Pic}`,
          x: 650,
          z: 0,
          zIndex: 2,
          ry: -45
        })
      }
      // 前七筆之後的參賽者資料
      requireData.forEach((item, index) => {
        if (index > 6) {
          if ($(window).width() > 768) {
            act_Data.push({
              registerNO: item.registration_NO,
              src: `./php/act/${item.registration_Pic}`,
              x: resizeCarousel * 4,
              z: 0,
              zIndex: 1,
              ry: 0
            })
          } else {
            act_Data.push({
              registerNO: item.registration_NO,
              src: `./php/act/${item.registration_Pic}`,
              x: 300,
              z: 0,
              zIndex: 1,
              ry: 0
            })
          }
        }
        // 注意 ticket 順序 因為跟indexs寫法有關
        if (index < 4) {
          ticket.push(item.votes)
        } else {
          ticket.splice(index - 4, 0, item.votes)
        }
        let ticketNo = document.querySelector('.ticketNo')
        ticketNo.innerText = ticket[ticket.length - 1 - index]
      })

      // 留言資料
      await $.ajax({
        url: './php/act/loadComment_act.php',
        type: 'post',
        dataType: 'JSON',
        data: JSON.stringify({ actNum: actNum }),
        success: function(commentDate) {
          chat = commentDate.concat(commentDate.splice(0, 4))
          // console.log(chat)
        },
        error: function(commentDate) {
          console.log('伺服器維護中...')
        }
      })

      X = []
      Z = []
      rY = []
      zIndex = []

      // 處理輪播
      renewCompetitor(act_Data)
      carousel(0)
    },
    error: function(requireData) {
      console.log('活動取資料')
      // alert('伺服器維護中...')
    }
  })

  //
  // 可報名的活動資料匯入
  await $.ajax({
    url: './php/act/loadActivity.php',
    type: 'POST',
    dataType: 'json',
    data: { getActNum: 0, actNum: actNum },
    success: function(actData) {
      // console.log(actData)
      let actDataString = ''
      actData.forEach((item, index) => {
        if (index !== 0) {
          actDataString = all_activity(
            actDataString,
            item.activity_Poster,
            item.activity_Name,
            item.activity_Info,
            item.activity_NO,
            item.maximum_num,
            item.signUp_NO,
            item.activity_Date
          )
        } else {
          // 變更活動圖片
          $('.act_poseRef')
            .find('img')
            .attr('src', `./php/act/${item.activity_Pic}`)
          $('.act_poseRefbelow1400px')
            .find('img')
            .attr('src', `./php/act/${item.activity_Pic}`)
          // 變更活動時間，標題與文字
          // 標題
          $('.act_actText')
            .find('div')
            .eq(0)
            .find('span')
            .eq(0)
            .text(`${item.activity_Name}`)
          // 日期
          let dd = new Date(item.activity_Date)
          dd.setDate(dd.getDate() + 13)
          $('.act_actText')
            .find('div')
            .eq(0)
            .find('span')
            .eq(1)
            .text(
              `${item.activity_Date.split('-')[1]}/${
                item.activity_Date.split('-')[2]
              } ~ ${dd.getMonth() + 1}/${dd.getDate()}`
            )
          // 說明
          $('.act_actText')
            .find('div')
            .eq(1)
            .text(`${item.activity_Info}`)
        }
      })
      $('.act_allActWrap').html(actDataString)
      // 報名跳窗顯示
      let signUpBtn = document.querySelectorAll('.signUpBtn')
      signUpBtn.forEach(item => {
        item.addEventListener('click', showSignUpPanel)
      })
    },
    error: function(actData) {
      // alert('伺服器維護中...請稍後!!')
      console.log('所有活動資料')
    }
  })

  //
  //
  //
  //
  //
  // 更多活動的卡片翻轉
  if ($(window).width() <= 768) {
    $('.act_details').addClass('below768px')
    $('.act_details').css('border-left', '2px solid #333')
    $('.act_eachAct').removeClass('upon768px')
    $('.imgBox').removeClass('upon768px')
    $('.act_eachAct').flip({
      trigger: 'click',
      speed: 500
    })
  }

  // 調整畫布長寬
  if ($(window).width() <= 768) {
    $('#myCanvas').attr('width', '360')
    $('#myCanvas').attr('height', '540')
  } else {
    $('#myCanvas').attr('width', '476')
    $('#myCanvas').attr('height', '666')
  }

  // 切換雕像位置
  if ($(window).width() <= 1400) {
    let isPoseRefbelow1400pxHidden = true
    $('.act_poseRefbelow1400px').click(function() {
      if (isPoseRefbelow1400pxHidden) {
        $('.act_poseRefbelow1400px').css('left', '-30px')
      } else {
        $('.act_poseRefbelow1400px').css('left', '-250px')
      }
      isPoseRefbelow1400pxHidden = !isPoseRefbelow1400pxHidden
    })
  }

  if ($(window).width() <= 768) {
    $('.act_carousel_All').css(
      'transform',
      `translateX(-${(768 - $(window).width()) / 2}px)`
    )
  }

  //
  // 左下清單漢堡開關
  let isOpenburgerList = true
  $('.burgerList').click(function() {
    if (isOpenburgerList) {
      toggleClassFun()
      $('.act_mask').css('display', 'block')
      // 展開後可被點擊
      $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
      $('.act_establishbelow768px').css('pointer-events', 'auto')
      $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
    } else {
      toggleClassFun()

      $('.act_mask').css('display', 'none')
      // 展開後可被點擊
      $('.act_moreactivitybelow768px').css('pointer-events', 'none')
      $('.act_establishbelow768px').css('pointer-events', 'none')
      $('.act_chatBoardbelow768px').css('pointer-events', 'none')
    }
    isOpenburgerList = !isOpenburgerList
  })

  // 更多活動
  $('.act_moreactivitybelow768px').click(function() {
    toggleClassFun()
    $('#act_ActivityContainer').css('display', 'block')
    $('.act_moreActivity').css('display', 'block')

    $('.burgerList').css('pointer-events', 'none')
    // 以防備circleBtn穿透被點擊到
    $('.act_moreactivitybelow768px').css('pointer-events', 'none')
    $('.act_establishbelow768px').css('pointer-events', 'none')
    $('.act_chatBoardbelow768px').css('pointer-events', 'none')
  })

  // 報名活動
  $('.act_establishbelow768px').click(function() {
    toggleClassFun()

    // 發起活動
    // 因為變成單獨跳窗所以標題字要改
    $('.act_ActivityContainerTopBar')
      .find('span')
      .attr('data-text', '發起活動')
    $('.act_ActivityContainerTopBar')
      .find('span')
      .text('發起活動')

    $('#act_ActivityContainer').css('display', 'block')
    $('.act_establishActivity').css('display', 'block')

    $('.burgerList').css('pointer-events', 'none')
    // 以防備circleBtn穿透被點擊到
    $('.act_moreactivitybelow768px').css('pointer-events', 'none')
    $('.act_establishbelow768px').css('pointer-events', 'none')
    $('.act_chatBoardbelow768px').css('pointer-events', 'none')
  })

  // 留言板
  $('.act_chatBoardbelow768px').click(function() {
    toggleClassFun()
    chatBoardCarousel()

    $('.act_chattingBoard').css('display', 'block')

    $('.burgerList').css('pointer-events', 'none')
    // 以防備circleBtn穿透被點擊到
    $('.act_moreactivitybelow768px').css('pointer-events', 'none')
    $('.act_establishbelow768px').css('pointer-events', 'none')
    $('.act_chatBoardbelow768px').css('pointer-events', 'none')

    window.addEventListener('keypress', showCommentByEnter)
  })

  // 關閉 跳窗
  $('.closeBtn').click(function() {
    $('.act_mask').css('display', 'none')
    // 恢復最初狀態
    $('#act_ActivityContainer').css('display', 'none')
    $('.act_moreActivity').css('display', 'none')
    $('.act_establishActivity').css('display', 'none')

    // 離開時清掉畫布(報名)上的東西
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
    let act_imgAdvice = document.querySelector('.act_imgAdvice')
    act_imgAdvice.children[0].innerText = ''

    // 清掉發起活動
    let act_showPic = document.querySelector('.act_showPic')
    act_showPic.innerHTML = ''
    let act_refImgName = document.querySelector('.act_refImgName')
    act_refImgName.innerText = ''

    // 恢復可被點擊
    $('.burgerList').css('pointer-events', 'auto')
    $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
    $('.act_establishbelow768px').css('pointer-events', 'auto')
    $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
    // 讓遮罩恢復
    isOpenburgerList = !isOpenburgerList
  })

  $('.act_commentCloseBtn').click(function() {
    $('.act_mask').css('display', 'none')
    $('.act_chattingBoard').css('display', 'none')

    // 恢復可被點擊
    $('.burgerList').css('pointer-events', 'auto')
    $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
    $('.act_establishbelow768px').css('pointer-events', 'auto')
    $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
    // 讓遮罩恢復
    isOpenburgerList = !isOpenburgerList

    window.removeEventListener('keypress', showCommentByEnter)
  })

  //
  //
  //
  //
  $(window).resize(async function() {
    // 調整輪播圖
    //
    act_Data = []
    ticket = []

    // 活動取資料
    // (參賽者、票數)
    await $.ajax({
      url: './php/act/loadActCompetitor.php',
      type: 'POST',
      dataType: 'JSON',
      data: JSON.stringify({ actNum: actNum }),
      success: function(requireData) {
        let resizeCarousel = $('.act_carousel_All').width() / 10
        // 前七筆參賽者資料
        if ($(window).width() > 768) {
          act_Data.push({
            registerNO: requireData[0].registration_NO,
            src: `./php/act/${requireData[0].registration_Pic}`,
            x: resizeCarousel * 0,
            z: 0,
            zIndex: 2,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[1].registration_NO,
            src: `./php/act/${requireData[1].registration_Pic}`,
            x: resizeCarousel * 1,
            z: 50,
            zIndex: 3,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[2].registration_NO,
            src: `./php/act/${requireData[2].registration_Pic}`,
            x: resizeCarousel * 2,
            z: 100,
            zIndex: 4,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[3].registration_NO,
            src: `./php/act/${requireData[3].registration_Pic}`,
            x: resizeCarousel * 4,
            z: 200,
            zIndex: 5,
            ry: 0
          })
          act_Data.push({
            registerNO: requireData[4].registration_NO,
            src: `./php/act/${requireData[4].registration_Pic}`,
            x: resizeCarousel * 6,
            z: 100,
            zIndex: 4,
            ry: -45
          })
          act_Data.push({
            registerNO: requireData[5].registration_NO,
            src: `./php/act/${requireData[5].registration_Pic}`,
            x: resizeCarousel * 7,
            z: 50,
            zIndex: 3,
            ry: -45
          })
          act_Data.push({
            registerNO: requireData[6].registration_NO,
            src: `./php/act/${requireData[6].registration_Pic}`,
            x: resizeCarousel * 8,
            z: 0,
            zIndex: 2,
            ry: -45
          })
        } else {
          act_Data.push({
            registerNO: requireData[0].registration_NO,
            src: `./php/act/${requireData[0].registration_Pic}`,
            x: -50,
            z: 0,
            zIndex: 2,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[1].registration_NO,
            src: `./php/act/${requireData[1].registration_Pic}`,
            x: -50,
            z: 50,
            zIndex: 3,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[2].registration_NO,
            src: `./php/act/${requireData[2].registration_Pic}`,
            x: 100,
            z: 100,
            zIndex: 4,
            ry: 45
          })
          act_Data.push({
            registerNO: requireData[3].registration_NO,
            src: `./php/act/${requireData[3].registration_Pic}`,
            x: 300,
            z: 250,
            zIndex: 5,
            ry: 0
          })
          act_Data.push({
            registerNO: requireData[4].registration_NO,
            src: `./php/act/${requireData[4].registration_Pic}`,
            x: 500,
            z: 100,
            zIndex: 4,
            ry: -45
          })
          act_Data.push({
            registerNO: requireData[5].registration_NO,
            src: `./php/act/${requireData[5].registration_Pic}`,
            x: 650,
            z: 50,
            zIndex: 3,
            ry: -45
          })
          act_Data.push({
            registerNO: requireData[6].registration_NO,
            src: `./php/act/${requireData[6].registration_Pic}`,
            x: 650,
            z: 0,
            zIndex: 2,
            ry: -45
          })
        }
        // 前七筆之後的參賽者資料
        requireData.forEach((item, index) => {
          if (index > 6) {
            if ($(window).width() > 768) {
              act_Data.push({
                registerNO: item.registration_NO,
                src: `./php/act/${item.registration_Pic}`,
                x: resizeCarousel * 4,
                z: 0,
                zIndex: 1,
                ry: 0
              })
            } else {
              act_Data.push({
                registerNO: item.registration_NO,
                src: `./php/act/${item.registration_Pic}`,
                x: 300,
                z: 0,
                zIndex: 1,
                ry: 0
              })
            }
          }
          // 注意 ticket 順序 因為跟indexs寫法有關
          if (index < 4) {
            ticket.push(item.votes)
          } else {
            ticket.splice(index - 4, 0, item.votes)
          }
        })
        X = []
        Z = []
        rY = []
        zIndex = []
        renewCompetitor(act_Data)
        carousel(0)
      },
      error: function(requireData) {
        alert('伺服器維護中...')
      }
    })

    //
    //
    //
    //
    //

    // 更多活動的卡片翻轉
    if ($(window).width() <= 768) {
      $('.act_details').addClass('below768px')
      $('.act_details').css('border-left', '2px solid #333')
      $('.act_eachAct').removeClass('upon768px')
      $('.imgBox').removeClass('upon768px')
      $('.act_eachAct').flip({
        trigger: 'click',
        speed: 500
      })
    }

    // 調整畫布長寬
    if ($(window).width() <= 768) {
      $('#myCanvas').attr('width', '320')
      $('#myCanvas').attr('height', '448')
    } else {
      $('#myCanvas').attr('width', '476')
      $('#myCanvas').attr('height', '666')
    }

    // 切換雕像位置
    if ($(window).width() <= 1400) {
      let isPoseRefbelow1400pxHidden = true
      $('.act_poseRefbelow1400px').click(function() {
        if (isPoseRefbelow1400pxHidden) {
          $('.act_poseRefbelow1400px').css('left', '-30px')
        } else {
          $('.act_poseRefbelow1400px').css('left', '-250px')
        }
        isPoseRefbelow1400pxHidden = !isPoseRefbelow1400pxHidden
      })
    }

    if ($(window).width() <= 768) {
      $('.act_carousel_All').css(
        'transform',
        `translateX(-${(768 - $(window).width()) / 2}px)`
      )
    }

    //
    // 左下清單漢堡開關
    let isOpenburgerList = true
    $('.burgerList').click(function() {
      if (isOpenburgerList) {
        toggleClassFun()
        $('.act_mask').css('display', 'block')
        // 展開後可被點擊
        $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
        $('.act_establishbelow768px').css('pointer-events', 'auto')
        $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
      } else {
        toggleClassFun()

        $('.act_mask').css('display', 'none')
        // 展開後可被點擊
        $('.act_moreactivitybelow768px').css('pointer-events', 'none')
        $('.act_establishbelow768px').css('pointer-events', 'none')
        $('.act_chatBoardbelow768px').css('pointer-events', 'none')
      }
      isOpenburgerList = !isOpenburgerList
    })

    // 更多活動
    $('.act_moreactivitybelow768px').click(function() {
      toggleClassFun()
      $('#act_ActivityContainer').css('display', 'block')
      $('.act_moreActivity').css('display', 'block')

      $('.burgerList').css('pointer-events', 'none')
      // 以防備circleBtn穿透被點擊到
      $('.act_moreactivitybelow768px').css('pointer-events', 'none')
      $('.act_establishbelow768px').css('pointer-events', 'none')
      $('.act_chatBoardbelow768px').css('pointer-events', 'none')
    })

    // 報名活動
    $('.act_establishbelow768px').click(function() {
      toggleClassFun()

      // 發起活動
      // 因為變成單獨跳窗所以標題字要改
      $('.act_ActivityContainerTopBar')
        .find('span')
        .attr('data-text', '發起活動')
      $('.act_ActivityContainerTopBar')
        .find('span')
        .text('發起活動')

      $('#act_ActivityContainer').css('display', 'block')
      $('.act_establishActivity').css('display', 'block')

      $('.burgerList').css('pointer-events', 'none')
      // 以防備circleBtn穿透被點擊到
      $('.act_moreactivitybelow768px').css('pointer-events', 'none')
      $('.act_establishbelow768px').css('pointer-events', 'none')
      $('.act_chatBoardbelow768px').css('pointer-events', 'none')
    })

    // 留言板
    $('.act_chatBoardbelow768px').click(function() {
      toggleClassFun()
      chatBoardCarousel()

      $('.act_chattingBoard').css('display', 'block')

      $('.burgerList').css('pointer-events', 'none')
      // 以防備circleBtn穿透被點擊到
      $('.act_moreactivitybelow768px').css('pointer-events', 'none')
      $('.act_establishbelow768px').css('pointer-events', 'none')
      $('.act_chatBoardbelow768px').css('pointer-events', 'none')

      window.addEventListener('keypress', showCommentByEnter)
    })

    // 關閉 跳窗
    $('.closeBtn').click(function() {
      $('.act_mask').css('display', 'none')
      // 恢復最初狀態
      $('#act_ActivityContainer').css('display', 'none')
      $('.act_moreActivity').css('display', 'none')
      $('.act_establishActivity').css('display', 'none')

      // 離開時清掉畫布(報名)上的東西
      ctx.clearRect(0, 0, myCanvas.width, myCanvas.height)
      let act_imgAdvice = document.querySelector('.act_imgAdvice')
      act_imgAdvice.children[0].innerText = ''

      // 清掉發起活動
      let act_showPic = document.querySelector('.act_showPic')
      act_showPic.innerHTML = ''
      let act_refImgName = document.querySelector('.act_refImgName')
      act_refImgName.innerText = ''

      // 恢復可被點擊
      $('.burgerList').css('pointer-events', 'auto')
      $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
      $('.act_establishbelow768px').css('pointer-events', 'auto')
      $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
      // 讓遮罩恢復
      isOpenburgerList = !isOpenburgerList
    })

    $('.act_commentCloseBtn').click(function() {
      $('.act_mask').css('display', 'none')
      $('.act_chattingBoard').css('display', 'none')

      // 恢復可被點擊
      $('.burgerList').css('pointer-events', 'auto')
      $('.act_moreactivitybelow768px').css('pointer-events', 'auto')
      $('.act_establishbelow768px').css('pointer-events', 'auto')
      $('.act_chatBoardbelow768px').css('pointer-events', 'auto')
      // 讓遮罩恢復
      isOpenburgerList = !isOpenburgerList

      window.removeEventListener('keypress', showCommentByEnter)
    })

    // window resize
  })
})
