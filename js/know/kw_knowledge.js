xx = new XMLHttpRequest()
xx.onload = function () {
  if (xx.status == 200) {
    let dataaa = JSON.parse(xx.responseText)
    // console.log(dataaa["mem_NO"]);
    mem_NONO = dataaa['mem_NO'] ? dataaa['mem_NO'] : -1
  } else {
    alert(xx.status)
  }
}
xx.open('get', './php/header.php', true)
xx.send(null)

//
//
if (mem_NONO != -1) {
  let xhr = new XMLHttpRequest()
  xhr.open('POST', './php/love_book.php', true)
  xhr.setRequestHeader('Content-type', 'application/json')
  xhr.send(JSON.stringify({
    isChangeFavorite: 0,
    mem_NO: mem_NONO
  }))
  xhr.onload = () => {
    data = JSON.parse(xhr.responseText)
    // console.log(xhr.responseText)
    if (data) {
      data.forEach(item => {
        $(`#booklove${item}`)
          .text('取消收藏')
          .css('background-color', '#f46f65')
          .attr('data-knowledge_no', 'state_0')
      })
    }
  }
}

//
//
//
$('#booklove1').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 1,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove1').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove1').text() == '收藏') {
      $('#booklove1')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove1')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})

$('#booklove2').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 2,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove2').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove2').text() == '收藏') {
      $('#booklove2')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove2')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove3').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 3,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove3').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove3').text() == '收藏') {
      $('#booklove3')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove3')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove4').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 4,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove4').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove4').text() == '收藏') {
      $('#booklove4')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove4')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove5').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 5,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove5').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove5').text() == '收藏') {
      $('#booklove5')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove5')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove6').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 6,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove6').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove6').text() == '收藏') {
      $('#booklove6')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove6')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove7').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 7,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove7').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove7').text() == '收藏') {
      $('#booklove7')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove7')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})
$('#booklove8').click(function (e) {
  if (mem_NONO != -1) {
    e.preventDefault()
    let a = $(this).attr('data-knowledge_no')
    a = a.split('_')[1]
    let xhr = new XMLHttpRequest()
    xhr.open('POST', './php/love_book.php', true)
    xhr.setRequestHeader('Content-type', 'application/json')
    xhr.send(
      JSON.stringify({
        my_Article: a,
        knowledge_NO: 8,
        mem_NO: mem_NONO,
        isChangeFavorite: 1
      })
    )
    xhr.onload = () => {
      console.log(xhr.responseText)
      let a = JSON.parse(xhr.responseText).my_Article
      console.log(a)
      $('#booklove8').attr('data-knowledge_no', `state_${a}`)
    }

    if ($('#booklove8').text() == '收藏') {
      $('#booklove8')
        .text('取消收藏')
        .css('background-color', '#f46f65')
    } else {
      $('#booklove8')
        .text('收藏')
        .css('background-color', '#ffffff')
    }
  } else {
    Swal.fire({
      html: '<div font-size: 24px;">還沒登入會員嗎?? 快去登入吧!</div>',
      showCancelButton: false,
      confirmButtonColor: '#f46f65',
      cancelButtonColor: '#aaa',
      confirmButtonText: '確定',
      // cancelButtonText: '取消',
      background: '#fff url("./img/p0080_m.jpg") center'
    }).then(result => {
      if (result.value) {
        // 這裡做按下確認後要做的事情
      }

    })
  }
})

// 機器人
var robotTime = 0

function robotTalk() {
  let arr = [
    '可以閱讀架上文章',
    '觀看健身相關影片',
    '算TDEE知道自己多胖ㄜ不，是說基礎代謝拉'
  ]
  if (robotTime < arr.length) {
    document.getElementById('forRobotTalk').innerText = arr[robotTime]
    robotTime++
  } else {
    robotTime = 0
  }
}
var t3 = new TimelineMax()
t3.to('.robotHead', 0, {
  y: -40,
  x: 0
})
t3.to('.robotHead', 1, {
  y: -80,
  x: 0
})
setInterval(robotTalk, 2000)