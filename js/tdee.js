TDEE_Calculate = document.querySelector('#TDEE_Calculate')
TDEE_Calculate.addEventListener("click",
    function (e) {
        e.preventDefault();
        var BMR;
        var TDEE;
        var sex = document.getElementById("sex").value; //性別
        var weight = parseInt(document.getElementById("weight").value); //體重KG
        var height = parseInt(document.getElementById("height").value); //身高CM
        var age = parseInt(document.getElementById("age").value); //年齡
        var activitylevel = document.getElementById("activitylevel").value; //日常活動等級
        var tdee_C = document.getElementById('tdee_C')
        //male BMR = 66 + ( 13.7 x KG)+ ( 5 x CM) - ( 6.8 x age)
        //Women BMR = 655 + ( 9.6 x KG) + ( 1.8 x CM) - ( 4.7 x age)
        if (sex == "1") {
            var val1 = 13.7 * weight;
            var val2 = 5 * height;
            var val3 = 6.8 * age;
            BMR = 66 + val1 + val2 - val3;
            var val4 = activitylevel;
        } else if (sex == "0") {
            var val1 = 9.6 * weight;
            var val2 = 1.8 * height;
            var val3 = 4.7 * age;
            BMR = 655 + val1 + val2 - val3;
            var val4 = activitylevel;
        }

        //TDEE = 數字x BMR
        switch (val4) {
            case "s": //沒在運動...下
                TDEE = BMR * 1.2;
                break;

            case "sm": //一個星期運動1-3天...中下
                TDEE = BMR * 1.375;
                break;

            case "m": //一個星期運動3-5天...中
                TDEE = BMR * 1.55;
                break;

            case "mh": //一個星期運動6-7天...中上
                TDEE = BMR * 1.725;
                break;

            case "h": // 無時無刻都在動...上
                TDEE = BMR * 1.9;
                break;
        }
        //document.write "Your BMR AND TDEE"
        document.getElementById('Result_BMR').innerHTML = '你的基礎代謝率： ' + BMR.toFixed(0)
        document.getElementById('Result1_BMR').innerHTML = '每日總消耗熱量TDEE：' + TDEE.toFixed(0)
        document.getElementsByName('body_Brm')[0].value = BMR.toFixed(0)
        document.getElementsByName('body_TDEE')[0].value = TDEE.toFixed(0)
        // 送資料
        let kw_tdeeForm = document.getElementById("kw_tdeeForm")
        let form = new FormData(kw_tdeeForm);
        let xhr = new XMLHttpRequest();
        xhr.open('post', 'http://localhost/DD104 G4/php/tdee.php', true);
        xhr.send(form);
        xhr.onload = function () {
            console.log(xhr.responseText)
        }
    }

)