  $(document).ready(function () {
    
  //後台登入表單驗證error message
  $('#adminForm').validate({
    focusInvalid: true,
    onkeyup: function (element, event) {
      //去除左右側空白
      var value = this.elementValue(element).trim();
      $(element).val(value);

    },
    
    submitHandler: function (form) {
      $.ajax({
        type: "POST",
        url: "./php/adminSignIn.php?",
        data: $(form).serialize(),
        dataType: "text", //接收到的資料型態
        crossDomain: true,
        success: function (data) {
          console.log(data);
          if (data == "登入成功") {
            Swal.fire({
              title: data,
              html: '<br><p>即將前往頁面</p>',
              showConfirmButton: false,
             background:'#fff url("./img/p0080_m.jpg")',
            });
            setTimeout(function () {
              location.href = 'backgroundManager.html';
               $("#h5out").html("登出");
            }, 1000)

          }  else {
            Swal.fire({
              title: "登入失敗",
              html: '<br><p>帳號密碼錯誤</p>',
              showConfirmButton: false,
              background:'#fff url("./img/p0080_m.jpg")',
            });
            setTimeout(function () {
              location.href = 'administrator.html';
            }, 1500)
          }

        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(thrownError);
          alert(ajaxOptions);
        }
      });
    }

  });


 

 });
