window.addEventListener('load', function () {
  // 點擊按鈕翻頁
  $('.btn_go').click(function () {
    if ($('.mem_Ad').hasClass('turn')) {
      $('#Login').addClass('loginshadow');
      $('.mem_Ad').removeClass('turn');
      $('#Signup').removeClass('Signupshadow');
    } else {
      $('.mem_Ad').addClass('turn');
      $('#Signup').addClass('Signupshadow');
      $('#Login').removeClass('loginshadow');

    };
  });

  // 尺寸770以下點擊立即註冊登入移動內容
  $('.textP').click(function () {
    $('div#Signup').css({
      zindex: '2',
      left: '0px',
    });
  });
  $('.text_L').click(function () {
    $('div#Signup').css({
      zindex: '3',
      left: '-1000px',
    });
  });

let clickpsw = document.getElementById("eye");
clickpsw.addEventListener("click",function () {
   let eye = document.getElementById("mem_Psw");
   if(eye.type === "password"){
     eye.type="text";
     this.innerHTML = '<path d = "M12,7c-2.48,0-4.5,2.02-4.5,4.5S9.52,16,12,16s4.5-2.02,4.5-4.5S14.48,7,12,7z M12,14.2c-1.49,0-2.7-1.21-2.7-2.7 c0-1.49,1.21-2.7,2.7-2.7s2.7,1.21,2.7,2.7C14.7,12.99,13.49,14.2,12,14.2z"></path> <path d = "M12,4C7,4,2.73,7.11,1,11.5C2.73,15.89,7,19,12,19s9.27-3.11,11-7.5C21.27,7.11,17,4,12,4z M12,17 c-3.79,0-7.17-2.13-8.82-5.5C4.83,8.13,8.21,6,12,6s7.17,2.13,8.82,5.5C19.17,14.87,15.79,17,12,17z"></path>';
    
   }else{
     eye.type="password";
   
     this.innerHTML = ` <path
              d="M10.58,7.25l1.56,1.56c1.38,0.07,2.47,1.17,2.54,2.54l1.56,1.56C16.4,12.47,16.5,12,16.5,11.5C16.5,9.02,14.48,7,12,7 C11.5,7,11.03,7.1,10.58,7.25z">
            </path>
            <path
              d="M12,6c3.79,0,7.17,2.13,8.82,5.5c-0.64,1.32-1.56,2.44-2.66,3.33l1.42,1.42c1.51-1.26,2.7-2.89,3.43-4.74 C21.27,7.11,17,4,12,4c-1.4,0-2.73,0.25-3.98,0.7L9.63,6.3C10.4,6.12,11.19,6,12,6z">
            </path>
            <path
              d="M16.43,15.93l-1.25-1.25l-1.27-1.27l-3.82-3.82L8.82,8.32L7.57,7.07L6.09,5.59L3.31,2.81L1.89,4.22l2.53,2.53 C2.92,8.02,1.73,9.64,1,11.5C2.73,15.89,7,19,12,19c1.4,0,2.73-0.25,3.98-0.7l4.3,4.3l1.41-1.41l-3.78-3.78L16.43,15.93z M11.86,14.19c-1.38-0.07-2.47-1.17-2.54-2.54L11.86,14.19z M12,17c-3.79,0-7.17-2.13-8.82-5.5c0.64-1.32,1.56-2.44,2.66-3.33 l1.91,1.91C7.6,10.53,7.5,11,7.5,11.5c0,2.48,2.02,4.5,4.5,4.5c0.5,0,0.97-0.1,1.42-0.25l0.95,0.95C13.6,16.88,12.81,17,12,17z">
            </path>`;
   }
});
 


  //登入表單驗證error message
  $('#SignIn_Form').validate({
    focusInvalid: true,
    onkeyup: function (element, event) {
      //去除左右側空白
      var value = this.elementValue(element).trim();
      $(element).val(value);

    },
    rules: {
      "mem_Email": {
        required: true,
        email: true
      },
      "mem_Psw": {
        required: true,
        minlength: 8,
      }
    },
    messages: {
      "mem_Email": {
        required: '必填',
        email: "請輸入正確格式",
      },
      "mem_Psw": {
        required: '必填',
        minlength: '不得少於8位數',
      },
    },

    errorPlacement: function (error, element) {
      var Error_msg = $(element).data('error');
      if (Error_msg) {
        $(Error_msg).append(error).css({
          color: "#dc4260",
          "margin-left": "20px"
        })
      } else {
        error.insertAfter(element);
      };

    },
    submitHandler: function (form) {
      $.ajax({
        type: "POST",
        url: "./php/SignIn.php?",
        data: $(form).serialize(),
        dataType: "text", //接收到的資料型態
        crossDomain: true,
        success: function (data) {
          if (data == "登入成功") {
            Swal.fire({
              title: data,
              html: '<br><p>即將前往頁面</p>',
              showConfirmButton: false,
              background: '#fff url("./img/p0080_m.jpg") center',
            });
            setTimeout(function () {
              window.location.href = document.referrer //跳轉並刷新
    
            }, 1000)


          } else if(data == "帳號停權") {
              Swal.fire({
                title: "登入失敗",
                html: '<br><p>此帳號已被停權，請洽管理員</p>',
                showConfirmButton: false,
                background: '#fff url("./img/p0080_m.jpg") center',
              });
          }else{
            Swal.fire({
              title: "登入失敗",
              html: '<br><p>帳號密碼錯誤</p>',
              showConfirmButton: false,
              background: '#fff url("./img/p0080_m.jpg") center',
            });
            setTimeout(function () {
               window.location.replace("login.html");
            
            }, 1500)
          }

        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(thrownError);
          alert(ajaxOptions);
        }
      });
    }

  });




  // 註冊表單驗證error message
  $('#Signup_Form').validate({
    focusInvalid: true,
    onkeyup: function (element, event) {
      //去除左右側空白
      var value = this.elementValue(element).trim();
      $(element).val(value);
    },
    rules: {
      "mem_Name": {
        required: true,
        minlength: 2

      },
      "mem_Nick_Name": {
        required: true,
      },
      "mem_Email": {
        required: true,
        email: true
      },
      "mem_Psw": {
        required: true,
        minlength: 8,
      }
    },
    messages: {
      "mem_Name": {
        required: '請填寫姓名',
        minlength: '最少2個字元'
      },
      "mem_Nick_Name": {
        required: '請填寫暱稱'
      },
      "mem_Email": {
        required: '必填',
        email: '請輸入正確格式'
      },
      "mem_Psw": {
        required: '必填',
        minlength: '不得少於8位數'
      },
    },
    errorPlacement: function (error, element) {
      var Error_msg = $(element).data('error');
      if (Error_msg) {
        $(Error_msg).append(error).css({
          color: "#dc4260",
          "margin-left": "20px"
        })
      } else {
        error.insertAfter(element);
      };
    },
    submitHandler: function (form) {
      $.ajax({
        type: "POST",
        url: "./php/SignUp.php?",
        data: $(form).serialize(),
        dataType: "text", //接收到的資料型態
        crossDomain: true,
        success: function (data) {
          if (data == "註冊成功") {
            Swal.fire({
              title: data,
              html: '<br><p>為您跳轉至首頁</p>',
              showConfirmButton: false,
              background: '#fff url("./img/p0080_m.jpg") center',
            });
            setTimeout(function () {
              window.location.replace("home.html");
            }, 1000)
           

          } else {
            Swal.fire({
              title: data,
              html: '<br><p>帳號已有人使用</p>',
              showConfirmButton: false,
              background: '#fff url("./img/p0080_m.jpg") center',
            });
            setTimeout(function () {
              window.location.replace("login.html");
            }, 1500)
          }

        },
        error: function (xhr, ajaxOptions, thrownError) {

          alert(xhr.status);
          alert(thrownError);
          alert(ajaxOptions);
        }
      })
    }

  });

  //忘記密碼輸入電子信箱 
  $('.mem_passw').click(function () {
    Swal.fire({
      title: '請輸入電子信箱',
      input: 'email',
      inputPlaceholder: '輸入電子信箱',
      confirmButtonText: '送出',
      confirmButtonColor: '#dc6247',
    }).then(function (email) {
      $("#alertemail").val(email.value);

      if ($("#alertemail").val() != "") {
        $.ajax({
          type: "POST",
          url: "./php/forgotPassword.php?",
          data: $("#alertemail").serialize(),
          dataType: "text", //接收到的資料型態
          crossDomain: true,
          success: function (data) {
            if (data == "已發送郵件") {
              Swal.fire({
                title: data,
                html: '<br><p>請至信箱收取郵件</p>',
                showConfirmButton: false,
                background: '#fff url("./img/p0080_m.jpg") center',
              });
              // setTimeout(function () {
              //  window.location.replace("login.html");
              // }, 3000)

            } else {
              Swal.fire({
                title: data,
                html: '<br><p>想想看🤔🧠</p>',
                showConfirmButton: false,
                background: '#fff url("./img/p0080_m.jpg") center',
                timer: 1000
              });
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
            alert(ajaxOptions);
          }
        });
      }
    })

  });

});
