window.onload = function () {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
        show(xhr.responseText);
    };
    xhr.open("get", "./php/body/backgroundGetItem.php", false);
    xhr.send(null);
}

function show(jsonStr) {
    var iii = JSON.parse(jsonStr);
    console.log(iii);


    var intml = "";
    for (var i in iii) {
        intml += `
    <div class="col-md-6 border border-primary mb-2">
    <div class="row mb-3">
        <label  class="col-sm-2 col-form-label">編號</label>
        <input type="text" class="form-control-plaintext col-sm-10 sportNoo" value="${iii[i].sport_No}">
    </div>
    <div class="row mb-3">
        <label  class="col-sm-2">名稱</label>
        <p>${iii[i].sport_Name}</p>
    </div>
    <div class="row mb-3">
        <label  class="col-sm-2">icon</label>
        <img src="${iii[i].icon}" height="70;" alt="" srcset="">
    </div>     
    <div class="row mb-3">
        <div class="col-sm-2">影片路徑</div>
       <p>${iii[i].video}</p>
    </div> 
    <div class="row mb-3">
        <div class="col-sm-2">訓練肌群</div>
       <p>${iii[i].info}</p>
    </div>     
    <div class="row mb-3">
        <div class="col-sm-2">顯示肌肉部位(圖像)</div>
        <img src="${iii[i].muscle}" height="100;"> 
      
    </div> 
    <div class="row mb-3">
        <div class="col-sm-2">顯示身體部位(圖像)</div>
        <img src="${iii[i].body}" height="70;"> 
      
    </div>  
    <div class="row mb-3">
        <div class="col-sm-2">運動器材(圖像)</div>
        <img src="${iii[i].item}" height="70;"> 
     
    </div> 
    <div class="row mb-3">
        <div class="col-sm-2">狀態</div>
        <select name="state" id="" >`
        if (iii[i].State == 0) {
            intml += `<option value="1">上架</option>
            <option selected value="0">下架</option>`
        } else {
            intml += `<option value="1">上架</option>
             <option value="0">下架</option>`
        }
        intml +=
            `</select>
        <div class="text-center col-sm-6 mx-auto">
           
            <button type="button" class="btn btn-primary update" ">確認修改</button>
        </div> 
    </div> 
    </div> 
    `;

    }
    document.querySelector(".contt").innerHTML = intml;

    for (let p = 0; p < document.querySelectorAll(".update").length; p++) {
        document.querySelectorAll(".update")[p].addEventListener("click", editEquipment, false);
    }
}



function editEquipment(e) {

    let upStatee = e.target.parentElement.parentElement.firstElementChild.nextElementSibling.value;
    let sportNoo = e.target.parentElement.parentElement.parentElement.firstElementChild.lastElementChild.value;
    let xhr = new XMLHttpRequest();

    xhr.onload = function () {
        if (xhr.status == 200) {
            // console.log(xhr.responseText);
            // alert("成功更新");
            Swal.fire({
                html: '<p style="font-size:18px">成功更新</p>',
                confirmButtonText: '確定',
                confirmButtonColor: '#f46f65',
                background: '#fff url("./img/p0080_m.jpg")'
            }).then(result => {
                if (result.value) {
                    location.reload();
                }
            })
            // location.reload();
        } else {
            alert(xhr.status);
        }
    };
    xhr.open("get", `./php/body/editEquipment.php?upState=${upStatee}&sportNo=${sportNoo}`, true);

    xhr.send(null);
}
