function doFirst() {
    $('.hamburger').click(function () {
        $(this).toggleClass('is-opened')
        $('.header_phone').slideToggle(1500)
    })
    $('#memout,#memout_m').click(function () {
        if ($('#memout,#memout_m').html() == '登入') {
            window.location.href = 'login.html'
        } else if ($('#memout,#memout_m').html() == '登出') {
            $.ajax({
                type: 'GET',
                url: './php/logout.php',
                dataType: 'text', //接收到的資料型態
                crossDomain: true,
                success: function (data) {
                    if (data) {
                        $('#memout,#memout_m').html('登入')
                        window.location.replace("home.html")
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status)
                    alert(thrownError)
                    alert(ajaxOptions)
                }
            })
        }
    })


    //知道是哪個會員近來
    $.ajax({
        type: "GET",
        url: "./php/header.php?",
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            if (data['mem_Email']) {
                $("#memout,#memout_m").html("登出");
                $(".iconmember").css({
                    "background-color": "#F8C12D",
                    "border-radius": "70%"
                });
            }
            let iamamiwhami = data['mem_NO'];
            // iamamiwhami =2;
            //將會員編號傳去所有要到PHP撈資料的地方
            getmempoweralldata(iamamiwhami);
            getmemtdeeyear(iamamiwhami);
            getmemhistroydata(iamamiwhami);
            getmemLovedata(iamamiwhami);
            getmemactdata(iamamiwhami);
            getmemcoupondata(iamamiwhami);
            getmemshopdata(iamamiwhami);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
            alert(ajaxOptions);
        }
    });

    //螢幕尺寸的
    memeSideResize();

    //機器人講話的動畫
    var t3 = new TimelineMax();
    t3.to('.robotHead', 0, {
        y: -40,
        x: 0,
    });
    t3.to('.robotHead', 1, {
        y: -80,
        x: 0,
    });
    setInterval(robotTalk, 2000);


    $(document).ready(function () {

        $('.timeline').scroll(function () {
            var items = $('li');

            let height = $(".timeline").height() + 100; //window可視範圍
            let windowOut = $(".timeline").scrollTop();
            $(items).each(function () {

                var items = $(this).offset().top;
                if (height + windowOut > items) {
                    $(this).addClass("in-view");
                } else {
                    $(this).removeClass("in-view");
                }
            });
        });

        $('input[type="file"]').change(function (e) {
            var fileName = e.target.files[0].name;
            $('#labelFile').html(fileName);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#mem_img').attr('src', e.target.result).css({
                        width: 250,
                        height: 250,
                    });
                    //___________保存按鈕__________________
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#modify_btn").click(function () {

            var fp = $("#mem_Pic");

            var items = fp[0].files;

            var fileName = items[0].name;
            // console.log(fileName);
            $.ajax({
                type: "GET",
                url: `./php/mem/UpDateimage.php?`,
                contentType: false,
                data: {
                    mem_Pic: `img/mem/images/${fileName}`
                },
                dataType: "text", //接收到的資料型態
                crossDomain: true,
                success: function (data) {
                    Swal.fire({
                        html: '<br><p>更新成功</p><br>',
                        showConfirmButton: false,
                        background: '#fff url("./img/p0080_m.jpg") center',
                        timer:2000
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                    alert(ajaxOptions);
                }
            });
        });

        $("#mem_Pic").change(function () {
            readURL(this);
        });

        //_________會員個人資料_________________
        $.ajax({
            type: "POST",
            url: "./php/mem/personal_information.php",
            dataType: "json", //接收到的資料型態
            crossDomain: true,
            success: function (data) {
                console.log(data['mem_Name']);
                $('#mem_Name').val(`${data['mem_Name']}`);
                $('#mem_Nick_Name').val(`${data['mem_Nick_Name']}`);
                $('#mem_Email').val(`${data['mem_Email']}`);
                $('#mem_Psw').val(`${data['mem_Psw']}`);
                $('#mem_img').attr('src', `${data['mem_Pic']}`);
                $('#body_Cm').val(`${data['body_Cm']}`);
                $('#body_Kg').val(`${data['body_Kg']}`);
                $('#body_Tdee').val(`${data['body_TDEE']}`);
                console.log($('#body_Cm').val());
                if (($('#body_Cm').val() || $('#body_Kg').val() || $('#body_Tdee').val()) == "null") {
                    $('#body_Cm').val("");
                    $('#body_Kg').val("");
                    $('#body_Tdee').val("");
                } else {
                    $('#body_Cm').val(`${data['body_Cm']}CM`);
                    $('#body_Kg').val(`${data['body_Kg']}KG`);
                    $('#body_Tdee').val(`${data['body_TDEE']}`);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
                alert(ajaxOptions);
                alert(xhr.status);
            }
        });
    });

    //一近來會員專區飛進來的效果
    let memtween = new TimelineMax();
    memtween.add(TweenMax.to(['#memLove,#menLovechange'], 2, {
        z: 0
    })).add(TweenMax.to(['#memActivity,#menActivitychange'], 0.3, {
        z: 0,
    })).add(TweenMax.to(['#memShop,#menShopchange'], 0.3, {
        z: 0,
    })).add(TweenMax.to(['#memInfo,#menInfochange'], 0.3, {
        z: 0,
    })).add(TweenMax.to(['#memBody,#menMybodychange'], 0.3, {
        z: 0,
    })).add(TweenMax.to(['#memSide'], 2, {
        ease: Elastic.easeOut,
        y: -60,
    }));
    TweenMax.staggerFrom(['.memBoard', '.memClip_1', '.memClip_2'], 2, {
        z: 500,
        x: 0,
        ease: Elastic.easeOut,
    }, 0.5);
    //飛進來效果結束

    // 當螢幕在770以下文字減少
    let memSideWidth = document.body;


    //點標籤的部分
    let menInfobtn = document.getElementById("menInfochange");
    let menmtbodybtn = document.getElementById("menMybodychange");
    let memShopbtn = document.getElementById("menShopchange");
    let memActivitybtn = document.getElementById("menActivitychange");
    let memLovebtn = document.getElementById("menLovechange");
    document.getElementById("memBody").style.zIndex = "11";
    document.getElementById("memInfo").style.zIndex = "10";
    document.getElementById("memShop").style.zIndex = "9";
    document.getElementById("memActivity").style.zIndex = "8";
    document.getElementById("memLove").style.zIndex = "7";
    menmtbodybtn.onclick = menmtbodytop;
    menInfobtn.onclick = menInfotop;
    memShopbtn.onclick = memShoptop;
    memActivitybtn.onclick = memActivitytop;
    memLovebtn.onclick = memLovetop;
    memSideWidth.onresize = memeSideResize;



}
//////////////////////////////////////////////////////////////////////////////////
// 當螢幕在770以下文字減少
function memeSideResize() {
    let bodywidth = document.body.clientWidth;
    if (bodywidth <= 770) {
        document.getElementById("menMybodychange").innerText = "body";
        document.getElementById("menInfochange").innerText = "個人";
        document.getElementById("menShopchange").innerText = "訂單";
        document.getElementById("menActivitychange").innerText = "活動";

    } else {
        document.getElementById("menMybodychange").innerText = "My body";
        document.getElementById("menInfochange").innerText = "會員資料";
        document.getElementById("menShopchange").innerText = "訂單與優惠券";
        document.getElementById("menActivitychange").innerText = "活動管理";
    }
}
//////////////////////////////////////////////////////////////////////////////////
//小機器人講話
var robotTime = 0;

function robotTalk() {
    let arr = ["改變從現在開始", "參與活動拿優惠券", "來看看你的成長吧"];
    if (robotTime < arr.length) {
        document.getElementById("forRobotTalk").innerText = arr[robotTime];
        robotTime++;
    } else {
        robotTime = 0;
    }
}
//點按標籤的部分/////////////////////////////////////////
function menInfotop() {
    if (document.getElementById("memInfo").style.zIndex == "11") {
        menmtbodybtn.onclick = "";
    }
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: -1
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menShopchange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memShop,#memActivity,#memLove', 0.3, {
        x: 200
    }));
    myTimeline.add(TweenLite.to('#memInfo,#menInfochange', 0.5, {
        x: -1000,
        y: 200,
        rotation: -20
    }));
    myTimeline.add(TweenLite.to('#memInfo', 0, {
        zIndex: 11
    }));
    myTimeline.add(TweenLite.to('#memBody', 0, {
        zIndex: 10
    }));
    myTimeline.add(TweenLite.to('#memShop', 0, {
        zIndex: 9
    }));
    myTimeline.add(TweenLite.to('#memActivity', 0, {
        zIndex: 8
    }));
    myTimeline.add(TweenLite.to('#memLove', 0, {
        zIndex: 7
    }));

    myTimeline.add(TweenLite.to('#memInfo,#menInfochange', 0.3, {
        x: 0,
        y: 0,
        rotation: 0
    }));

    myTimeline.add(TweenLite.to('#menMybodychange,#menShopchange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memShop,#memActivity,#memLove', 0.1, {
        x: 0
    }));
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: 12
    }));

    clipkaikai();
    setTimeout(removeclipclose, 1400);


}

function menmtbodytop() {
    if (document.getElementById("memBody").style.zIndex == "11") {
        menmtbodybtn.onclick = "";
    }
    document.getElementById("memBody").style.zIndex = "6";
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: -1
    }));
    myTimeline.add(TweenLite.to('#menInfochange,#menShopchange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memInfo,#memShop,#memActivity,#memLove', 0.3, {
        x: 200
    }));
    myTimeline.add(TweenLite.to('#memBody,#menMybodychange', 0.5, {
        x: -1000,
        y: 200,
        rotation: -20
    }));
    myTimeline.add(TweenLite.to('#memBody', 0, {
        zIndex: 11
    }));
    myTimeline.add(TweenLite.to('#memInfo', 0, {
        zIndex: 10
    }));

    myTimeline.add(TweenLite.to('#memShop', 0, {
        zIndex: 9
    }));
    myTimeline.add(TweenLite.to('#memActivity', 0, {
        zIndex: 8
    }));
    myTimeline.add(TweenLite.to('#memLove', 0, {
        zIndex: 7
    }));

    myTimeline.add(TweenLite.to('#memBody,#menMybodychange', 0.3, {
        x: 0,
        y: 0,
        rotation: 0
    }));
    myTimeline.add(TweenLite.to('#menInfochange,#menShopchange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memInfo,#memShop,#memActivity,#memLove', 0.3, {
        x: 0
    }));
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: 12
    }));

    clipkaikai();
    setTimeout(removeclipclose, 1400);

}

function memShoptop() {
    if (document.getElementById("memShop").style.zIndex == "11") {
        menmtbodybtn.onclick = "";
    }
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: -1
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memActivity,#memLove', 0.3, {
        x: 200
    }));
    myTimeline.add(TweenLite.to('#memShop,#menShopchange', 0.5, {
        x: -1000,
        y: 200,
        rotation: -20
    }));
    myTimeline.add(TweenLite.to('#memShop', 0, {
        zIndex: 11
    }));
    myTimeline.add(TweenLite.to('#memBody', 0, {
        zIndex: 10
    }));
    myTimeline.add(TweenLite.to('#memInfo', 0, {
        zIndex: 9
    }));

    myTimeline.add(TweenLite.to('#memActivity', 0, {
        zIndex: 8
    }));
    myTimeline.add(TweenLite.to('#memLove', 0, {
        zIndex: 7
    }));

    myTimeline.add(TweenLite.to('#memShop,#menShopchange', 0.3, {
        x: 0,
        y: 0,
        rotation: 0
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menActivitychange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memActivity,#memLove', 0.3, {
        x: 0
    }));
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: 12
    }));

    clipkaikai();
    setTimeout(removeclipclose, 1400);

}

function memActivitytop() {
    if (document.getElementById("memActivity").style.zIndex == "11") {
        menmtbodybtn.onclick = "";
    }
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: -1
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menShopchange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memShop,#memLove', 0.3, {
        x: 200
    }));
    myTimeline.add(TweenLite.to('#memActivity,#menActivitychange', 0.5, {
        x: -1000,
        y: 200,
        rotation: -20
    }));
    myTimeline.add(TweenLite.to('#memActivity', 0, {
        zIndex: 11
    }));

    myTimeline.add(TweenLite.to('#memBody', 0, {
        zIndex: 9
    }));
    myTimeline.add(TweenLite.to('#memInfo', 0, {
        zIndex: 8
    }));
    myTimeline.add(TweenLite.to('#memShop', 0, {
        zIndex: 10
    }));
    myTimeline.add(TweenLite.to('#memLove', 0, {
        zIndex: 7
    }));
    myTimeline.add(TweenLite.to('#memActivity,#menActivitychange', 0.3, {
        x: 0,
        y: 0,
        rotation: 0
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menShopchange,#menLovechange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memShop,#memLove', 0.3, {
        x: 0
    }));
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: 12
    }));



    clipkaikai();
    setTimeout(removeclipclose, 1400);


}

function memLovetop() {
    if (document.getElementById("memLove").style.zIndex == "11") {
        menmtbodybtn.onclick = "";
    }
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: -1
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menShopchange,#menActivitychange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memShop,#memActivity', 0.3, {
        x: 200
    }));
    myTimeline.add(TweenLite.to('#memLove,#menLovechange', 0.5, {
        x: -1000,
        y: 200,
        rotation: -20
    }));
    myTimeline.add(TweenLite.to('#memLove', 0, {
        zIndex: 11
    }));
    myTimeline.add(TweenLite.to('#memBody', 0, {
        zIndex: 10
    }));
    myTimeline.add(TweenLite.to('#memInfo', 0, {
        zIndex: 9
    }));
    myTimeline.add(TweenLite.to('#memShop', 0, {
        zIndex: 8
    }));

    myTimeline.add(TweenLite.to('#memActivity', 0, {
        zIndex: 7
    }));



    myTimeline.add(TweenLite.to('#memLove,#menLovechange', 0.3, {
        x: 0,
        y: 0,
        rotation: 0
    }));
    myTimeline.add(TweenLite.to('#menMybodychange,#menInfochange,#menShopchange,#menActivitychange,.memClip_2,.memClip_1,.memBoard,#memBody,#memInfo,#memShop,#memActivity', 0.3, {
        x: 0
    }));
    myTimeline.add(TweenLite.to('#anno', 0.1, {
        zIndex: 12
    }));


    clipkaikai();
    setTimeout(removeclipclose, 1400);


}
//////////////////////////////////////////////////////////////////////////////////
//讓夾子開合
function clipkaikai() {
    var myTimeline = new TimelineMax();
    myTimeline.add(TweenLite.to('.memClip_2', 0.3, {
        y: 5,
        repeat: 1,
        yoyo: true
    }));
    let aa = document.getElementsByClassName("memClip_2")[0];
    aa.classList.add("clipshadow");
}
//夾子結束開合
function removeclipclose() {
    let aa = document.getElementsByClassName("memClip_2")[0];
    aa.classList.remove("clipshadow");
}

//////////////////////////////////////////////////////////////////////////////////
//能力值呈現
function showmempoweralldata(jsonp) {
    let jsonpData = JSON.parse(jsonp);
    //定義變數
    let chartRadarDOM;
    let chartRadarData;
    let chartRadarOptions;

    //載入雷達圖
    Chart.defaults.global.legend.display = false;
    Chart.defaults.global.defaultFontColor = 'rgba(0,0,74, 1)';
    chartRadarDOM = document.getElementById("chartRadar");
    chartRadarData;
    chartRadarOptions = {
        scale: {
            ticks: {
                fontSize: 16,
                beginAtZero: true,
                maxTicksLimit: 7,
                min: 0,
                max: 100
            },
            pointLabels: {
                fontSize: 18.72,
                color: '#0044BB',
                fontFamily: 'Noto Sans TC'
            },
            gridLines: {
                color: 'rgba(244,111,101,.8)'
            }
        }
    };
    if (document.body.clientWidth <= 414) {
        chartRadarOptions = {
            scale: {
                ticks: {
                    fontSize: 10,
                    beginAtZero: true,
                    maxTicksLimit: 7,
                    min: 0,
                    max: 100
                },
                pointLabels: {
                    fontSize: 14,
                    color: '#0044BB',
                    fontFamily: 'Noto Sans TC'
                },
                gridLines: {
                    color: 'rgba(244,111,101,.8)'
                }
            }
        }
    }

    ///////////////////////之後在這裡抓值進去
    let doordonot = jsonpData[0];
    let applyactivity = jsonpData[1];
    let byecal = jsonpData[2];
    let aaa = jsonpData[3];
    let lucky = jsonpData[4];
    if (doordonot >= 100) {
        doordonot = 100;
    }
    if (applyactivity >= 100) {
        applyactivity = 100;
    }
    if (byecal >= 100) {
        byecal = 100;
    }
    if (aaa >= 100) {
        aaa = 100;
    }
    if (byecal >= 100) {
        byecal = 100;
    }
    if (lucky >= 100) {
        lucky = 100;
    }
    ////////////////////////////////////////
    let graphData = new Array();
    graphData.push(doordonot); //執行力
    graphData.push(applyactivity); //活躍度
    graphData.push(byecal); //耐力
    graphData.push(aaa); //討論力
    graphData.push(lucky); //幸運值

    //CreateData
    chartRadarData = {
        labels: ['執行力', '活躍度', '耐力', '討論力', '幸運值'],
        datasets: [{
            label: "(%)",
            backgroundColor: "rgba(220, 98, 71,0.7)",
            borderColor: "rgba(244,111,101,.8)",
            pointBackgroundColor: "rgb(238, 193, 109)",
            pointBorderColor: "rgb(238, 193, 109)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(0,0,0,0.5)",
            pointBorderWidth: 5,
            data: graphData
        }]
    };

    //Draw(畫)
    let chartRadar = new Chart(chartRadarDOM, {
        type: 'radar',
        data: chartRadarData,
        options: chartRadarOptions
    });

    document.querySelector(".memBodyPart_1awords").innerHTML = "執行力:" + doordonot + "%<br>活躍度:" +
        applyactivity + "%<br>耐力:" + byecal + "%<br>討論力:" + aaa + "%<br>幸運度:" + lucky + "%<br>";

}
//撈能力值
function getmempoweralldata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmempoweralldata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memallpower.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//TDEE圖表資料呈現
function showmemtdeeyearr(jsonStr) {
    let jsonStrData = JSON.parse(jsonStr);
    let memTdeeYear = jsonStrData.year;
    let memTdeeData = jsonStrData.data;
    //取這個會員Tdee有座的年份放進選單中
    let html = "";
    for (var i = Object.keys(memTdeeYear).length; i > 0; i--) {
        html += `<option>${memTdeeYear[i]}</option>`;
    }
    let teddaMnum = document.getElementById("tdeecontent");
    teddaMnum.innerHTML = html;

    //把最新的TDEE顯示出來
    let tdeeM;
    tdeeM = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

    for (var i in memTdeeData[memTdeeYear[Object.keys(memTdeeYear).length]]) {
        tdeeM.splice(i - 1, i, memTdeeData[memTdeeYear[Object.keys(memTdeeYear).length]][i]);
    }

    //沒填Tdee的時候會跟上一個月一樣
    for (var j = 0; j <= tdeeM.length; j++) {
        if (tdeeM.indexOf(0) != -1) {
            tdeeM[tdeeM.indexOf(0)] = tdeeM[tdeeM.indexOf(0) - 1];
        }
    }

    //變成圖表
    new Chart(document.getElementById("line-chart"), {
        type: 'line',
        data: {
            labels: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            datasets: [{
                data: [tdeeM[0], tdeeM[1], tdeeM[2], tdeeM[3], tdeeM[4], tdeeM[5], tdeeM[6], tdeeM[7], tdeeM[8], tdeeM[9], tdeeM[10], tdeeM[11]], //抓tdee
                label: "TDEE",
                borderColor: "rgb(238, 193, 109)",
                fill: true,
                backgroundColor: "rgb(238, 193, 109,0.2)",
                pointBackgroundColor: "#f46f65",
                pointBorderColor: "#f46f65",
                pointBorderWidth: 5
            }]
        },
    });

    //改年份要出現該年分的TDEE    
    teddaMnum.addEventListener("change", function () {


        tdeeM = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        for (var i in memTdeeData[teddaMnum.value]) {
            tdeeM.splice(i - 1, i, memTdeeData[teddaMnum.value][i]);

        }

        //沒填Tdee的時候會跟上一個月一樣
        for (var j = 0; j <= tdeeM.length; j++) {
            if (tdeeM.indexOf(0) != -1) {
                tdeeM[tdeeM.indexOf(0)] = tdeeM[tdeeM.indexOf(0) - 1];
            }
        }
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                datasets: [{
                    data: [tdeeM[0], tdeeM[1], tdeeM[2], tdeeM[3], tdeeM[4], tdeeM[5], tdeeM[6], tdeeM[7], tdeeM[8], tdeeM[9], tdeeM[10], tdeeM[11]], //抓tdee
                    label: "TDEE",
                    borderColor: "rgb(238, 193, 109)",
                    fill: true,
                    backgroundColor: "rgb(238, 193, 109,0.2)",
                    pointBackgroundColor: "#f46f65",
                    pointBorderColor: "#f46f65",
                    pointBorderWidth: 5
                }]
            },

        });

    });
    //////////////////////////////////////



}
//接收TDEE圖表資料呈現 
function getmemtdeeyear(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmemtdeeyearr(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memTdeeYear.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//歷史訓練菜單的呈現
function showmemhistroydata(jsonStrhis) {
    let jsonStrhisData = JSON.parse(jsonStrhis);
    let memhisDataYear = jsonStrhisData.year;
    let memhisDataMonth = jsonStrhisData.month;
    let memhisDataDay = jsonStrhisData.day;
    let memhisDatasportname = jsonStrhisData.sportnamedata;
    let memhisDatagroup = jsonStrhisData.groupNumdata;
    let memhisDatatimes = jsonStrhisData.timesNumdata;
    let memhisDatakg = jsonStrhisData.kgdata;
    //放年進去
    let htmlhis = "";
    for (var i in memhisDataYear) {
        htmlhis += `<option>${memhisDataYear[i]}</option>`;
    }
    let hsimenuyear = document.getElementById("trinallcontentyear");
    hsimenuyear.innerHTML = htmlhis;
    var trinallcontentmomall = document.getElementById("trinallcontent");
    var trinallcontentyear = document.getElementById("trinallcontentyear");
    //月份選單
    //還有預選當前月份
    //如果沒有紀錄的月份就不能點選
    for (var i = 12; i > 0; i--) {
        if (i == Object.values(memhisDataMonth)[0]) {
            trinallcontentmomall = document.getElementById("trinallcontent");
            trinallcontentmom = document.createElement("option");
            trinallcontentmom.setAttribute("value", i);
            trinallcontentmom.setAttribute("selected", "selected");
            trinallcontentTextnode = document.createTextNode(i + "月");
            trinallcontentmom.appendChild(trinallcontentTextnode);
            trinallcontentmomall.appendChild(trinallcontentmom);
        } else {
            if (Object.values(memhisDataMonth[memhisDataYear[1]]).indexOf(`${i}`) != -1) {
                trinallcontentmomall = document.getElementById("trinallcontent");
                trinallcontentmom = document.createElement("option");
                trinallcontentmom.setAttribute("value", i);
                trinallcontentTextnode = document.createTextNode(i + "月");
                trinallcontentmom.appendChild(trinallcontentTextnode);
                trinallcontentmomall.appendChild(trinallcontentmom);
            } else {
                trinallcontentmomall = document.getElementById("trinallcontent");
                trinallcontentmom = document.createElement("option");
                trinallcontentmom.setAttribute("value", i);
                trinallcontentmom.setAttribute("disabled", "disabled");
                trinallcontentTextnode = document.createTextNode(i + "月");
                trinallcontentmom.appendChild(trinallcontentTextnode);
                trinallcontentmomall.appendChild(trinallcontentmom);
            }
        }
    };
    //最新這個月的資料
    let memhisdaytthtml = "";
    let hsimenudayall = document.querySelector(".memtranallday");
    for (var p in memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]]) {
        memhisdaytthtml += `
     <div class="memtranweek">
     <div class="memtranamom">
       ${memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]}
     </div>
     <div class="memtranweekword" id="scrollbarwithe">
    <p class="memtranweekworddate">${memhisDataYear[1]}/${Object.values(memhisDataMonth[memhisDataYear[1]])[0]}/${memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]}</p>
    <hr>
     `
        for (var t in memhisDatasportname[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]]) {
            memhisdaytthtml += `
           <p>${memhisDatasportname[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t]}</p>
           <p><span>${memhisDatagroup[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t][memhisDatasportname[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t]]}</span>組<span>${memhisDatatimes[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t][memhisDatasportname[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t]]}</span>次<span>${memhisDatakg[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t][memhisDatasportname[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][memhisDataDay[memhisDataYear[1]][Object.values(memhisDataMonth[memhisDataYear[1]])[0]][p]][t]]}</span>KG</p>
           <hr>
         `
        }
        memhisdaytthtml += `
        </div>
      </div>
           `;
        hsimenudayall.innerHTML = memhisdaytthtml;
    }
    //點按年份轉換
    trinallcontentyear.addEventListener("change", function () {
        trinallcontentmomall.innerHTML = "";
        for (var i = 12; i > 0; i--) {
            if (Object.values(memhisDataMonth[trinallcontentyear.value]).indexOf(`${i}`) != -1) {
                trinallcontentmomall = document.getElementById("trinallcontent");
                trinallcontentmom = document.createElement("option");
                trinallcontentmom.setAttribute("value", i);
                trinallcontentTextnode = document.createTextNode(i + "月");
                trinallcontentmom.appendChild(trinallcontentTextnode);
                trinallcontentmomall.appendChild(trinallcontentmom);
            } else {
                trinallcontentmomall = document.getElementById("trinallcontent");
                trinallcontentmom = document.createElement("option");
                trinallcontentmom.setAttribute("value", i);
                trinallcontentmom.setAttribute("disabled", "disabled");
                trinallcontentTextnode = document.createTextNode(i + "月");
                trinallcontentmom.appendChild(trinallcontentTextnode);
                trinallcontentmomall.appendChild(trinallcontentmom);
            }
        };
        if (Object.values(memhisDataMonth[trinallcontentyear.value]).indexOf(trinallcontentmomall.value) == -1) {
            hsimenudayall.innerHTML = "";
        } else {
            memhisdaytthtml = "";
            for (var p in memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value]) {
                memhisdaytthtml += `
         <div class="memtranweek">
         <div class="memtranamom">
           ${memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]}
         </div>
         <div class="memtranweekword" id="scrollbarwithe">
        <p class="memtranweekworddate">${trinallcontentyear.value}/${trinallcontentmomall.value}/${memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]}</p>
        <hr>
         `
                for (var t in memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]]) {
                    memhisdaytthtml += `
               <p>${memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]}</p>
               <p><span>${memhisDatagroup[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>組<span>${memhisDatatimes[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>次<span>${memhisDatakg[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>KG</p>
               <hr>
             `
                }
                memhisdaytthtml += `
            </div>
          </div>
               `;
                hsimenudayall.innerHTML = memhisdaytthtml;
            }
        }
        //動畫 
        let trinallcontenttween = new TimelineMax();
        trinallcontenttween.add(TweenLite.to('.memtranweek', 0.5, {
            scale: 0,
            opacity: 0

        }));
        trinallcontenttween.add(TweenLite.to('.memtranweek', 0.5, {
            opacity: 1,
            scale: 1
        }));
    });
    //點選月份轉換
    trinallcontentmomall.addEventListener("change", function () {
        if (Object.values(memhisDataMonth[trinallcontentyear.value]).indexOf(trinallcontentmomall.value) == -1) {
            hsimenudayall.innerHTML = "";
        } else {
            memhisdaytthtml = "";
            for (var p in memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value]) {
                memhisdaytthtml += `
             <div class="memtranweek">
             <div class="memtranamom">
               ${memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]}
             </div>
             <div class="memtranweekword" id="scrollbarwithe">
            <p class="memtranweekworddate">${trinallcontentyear.value}/${trinallcontentmomall.value}/${memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]}</p>
            <hr>
             `
                for (var t in memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]]) {
                    memhisdaytthtml += `
                   <p>${memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]}</p>
                   <p><span>${memhisDatagroup[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>組<span>${memhisDatatimes[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>次<span>${memhisDatakg[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t][memhisDatasportname[trinallcontentyear.value][trinallcontentmomall.value][memhisDataDay[trinallcontentyear.value][trinallcontentmomall.value][p]][t]]}</span>KG</p>
                   <hr>
                 `
                }
                memhisdaytthtml += `
                </div>
              </div>
                   `;
                hsimenudayall.innerHTML = memhisdaytthtml;
            }
        }
        //動畫
        let trinallcontenttween = new TimelineMax();
        trinallcontenttween.add(TweenLite.to('.memtranweek', 0.5, {
            scale: 0,
            opacity: 0

        }));
        trinallcontenttween.add(TweenLite.to('.memtranweek', 0.5, {
            opacity: 1,
            scale: 1
        }));
    });
}
//拿歷史訓練菜單的東西
function getmemhistroydata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmemhistroydata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memHistoryMenu.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//show活動管理
function showmemactdata(jsonActh) {
    let jsonActhData = JSON.parse(jsonActh);
    console.log(jsonActhData);
    console.log(Object.keys(jsonActhData).length);
    // console.log(Object.values(Object.values(jsonActhData)[0])[0]);
    console.log(Object.keys(jsonActhData)[1]);
    //console.log(Object.keys(Object.values(jsonActhData)[1])[0].split("_")[0]);
    // console.log(Object.values(Object.values(jsonActhData)[1])[0]);
    let memtimeline = document.querySelector(".timeline");
    var htmlmemact = "";
    htmlmemact += `<ul>`
    for (var i = 0; i < Object.keys(jsonActhData).length; i++) {
       for(var j = 0;j<Object.keys(Object.values(jsonActhData)[i]).length;j++){
            htmlmemact += `
        <li class="in-view">
           <div class="text">
                 <p class="time">${Object.keys(jsonActhData)[i]}</p>
                 `
        if (Object.keys(Object.values(jsonActhData)[i])[j].split("_")[0] == "est") {
            htmlmemact += `
                 <p class="Act_text">舉辦活動:${Object.values(Object.values(jsonActhData)[i])[j]}</p>
                 `
        } else if (Object.keys(Object.values(jsonActhData)[i])[j].split("_")[0] == "regi") {
            htmlmemact += `
                    <p class="Act_text">參加活動:${Object.values(Object.values(jsonActhData)[i])[j]}</p>
                    `
        }

        htmlmemact += `
            <div class="Act_content">
                    <img src="./php/act/${Object.keys(Object.values(jsonActhData)[i])[j]}">
               
            </div>
          </div>
        </li>
        `;
       };
        

       
    }
    htmlmemact += `</ul>`
    memtimeline.innerHTML = htmlmemact;

}
//撈活動管理
function getmemactdata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmemactdata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memacthis.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//秀出我的收藏
function showmemLovedata(jsonLove, iamamiwhami) {

    let jsonLoveData = JSON.parse(jsonLove);
    let jsonLoveDatatitle = jsonLoveData.ktitlename;
    let jsonLoveDatabook = jsonLoveData.kpicurlbook;
    let jsonLoveDatapicurl = jsonLoveData.kpicurl;
    var C_love = document.querySelector(".C_love");
    let htmllovek = "";
    for (var i in jsonLoveDatatitle) {
        var bookmem = jsonLoveDatabook[i];

        htmllovek += `
    <div class="memeLove_Content">
    <a onclick="gotoknowledge(${bookmem})" style = "cursor: pointer;"><img class="book_img" src="${jsonLoveDatapicurl[jsonLoveDatatitle[i]]}"></a>
    <p class="memLove_book">${jsonLoveDatatitle[i]}</p>
    <a onclick="removebyebyememLove(${bookmem},${iamamiwhami})" style = "cursor: pointer;"><img id="Cancel_love" src="./img/mem/like.png" title="取消收藏"></a>
    </div>
    `;
    }

    C_love.innerHTML = htmllovek;

}
//移除收藏
function removebyebyememLove(e, iamamiwhami) {
    // console.log(e,iamamiwhami);
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
                    Swal.fire({
                        html: '<p>取消收藏</div>',
                        confirmButtonText: '確定',
                        confirmButtonColor: '#f46f65',
                        background: '#fff url("./img/p0080_m.jpg") center'
                    }).then(result => {
                        if (result.value) {
                            location.reload();
                        }
                    })
            // alert("取消收藏");
            // location.reload();
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/membyelove.php?mem_NO=" + iamamiwhami + "&" + "knowledge_NO=" + e;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//拿收藏
function getmemLovedata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmemLovedata(xhr.responseText, iamamiwhami);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memLove.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//呈現優惠券的地方
function showmmemcoupondata(jsoncon) {
    let jsonconData = JSON.parse(jsoncon);
    // console.log(jsonconData['cpic'][0]);
    if (jsonconData.length == 0) {

    } else {
        var mem_Coupon_img = document.querySelector(".mem_Coupon_img");
        var htmlmemCouponimg = "";
        for (var i in jsonconData['cpic']) {
            // console.log(i);
            htmlmemCouponimg += `
    <a href="shop.html"><img src="./img${jsonconData['cpic'][i]}" alt="優惠券"></a>
    <p style="margin-left:15px">優惠券編號:${jsonconData['cnum'][i]}</p>
    `;
        }
        mem_Coupon_img.innerHTML = htmlmemCouponimg;
    }


}
//撈優惠券
function getmemcoupondata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmmemcoupondata(xhr.responseText);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memcoupon.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}
//呈現訂單
function showmmemshopdata(jsonshop, iamamiwhami) {
    var bodywidthA = document.body.clientWidth;
    if (bodywidthA <= 770) {

        var jsonshopData = JSON.parse(jsonshop);
        var ordDatamemA = document.querySelector(".Ord_table_2");
        var htmlordDatamemA = "";

        for (var u in jsonshopData) {
            htmlordDatamemA += `
    
    <div class="ordnum" >日期:[${jsonshopData[u]['order_time']}]</div>
    <div class="ordmony">金額:NT$[${jsonshopData[u]['order_amount']}]</div>
   
   `
            if (jsonshopData[u]['shipping_status'] == 0) {
                htmlordDatamemA +=
                    ` <div class="ords">狀態:[尚未付款]</div>
         <div class="orddate">
        <input onclick="btnshopbye(${jsonshopData[u]['order_no']},${iamamiwhami})" type="button" value="取消訂單">
        </div>
        <br>
        `
            } else if (jsonshopData[u]['shipping_status'] == 1) {
                htmlordDatamemA +=
                    `<div class="ords">狀態:[處理中]</div>
        <div class="orddate">
       <input onclick="btnshopbye(${jsonshopData[u]['order_no']},${iamamiwhami})" type="button" value="取消訂單">
       </div>
       <br>
        `
            }



        }
        ordDatamemA.innerHTML = htmlordDatamemA;



    } else {
        var jsonshopData = JSON.parse(jsonshop);
        var ordDatamem = document.getElementById("ordDatamem");
        var htmlordDatamem = "";
        htmlordDatamem += `
    <tr class="Ord_title">
    <th class="Ord_th">訂單日期</th>
    <th class="Ord_th">總金額</th>
    <th class="Ord_th">訂單狀態</th>
    <th class="Ord_th"></th>
    </tr>`
        for (var u in jsonshopData) {
            htmlordDatamem += `
    <tr class="Ord_title">
    <td class="Ord_td">${jsonshopData[u]['order_time']}</td>
    <td class="Ord_td">${jsonshopData[u]['order_amount']}</td>
    `
            if (jsonshopData[u]['shipping_status'] == 0) {
                htmlordDatamem +=
                    `<td class="Ord_td">尚未付款</td>
        <td class="Ord_td"><input onclick="btnshopbye(${jsonshopData[u]['order_no']},${iamamiwhami})" type="button" value="取消訂單"></td>
        `
            } else if (jsonshopData[u]['shipping_status'] == 1) {
                htmlordDatamem +=
                    `<td class="Ord_td">處理中</td>
        <td class="Ord_td"><input onclick="btnshopbye(${jsonshopData[u]['order_no']},${iamamiwhami})" type="button" value="取消訂單"></td>
        </tr>
        `
            }
        }
        ordDatamem.innerHTML = htmlordDatamem;
    }


}
//取消訂單
function btnshopbye(e, iamamiwhami) {
    // console.log(e, iamamiwhami);
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            // alert("取消訂單");
            // location.reload();
            Swal.fire({
                html: '<p>取消訂單</div>',
                confirmButtonText: '確定',
                confirmButtonColor: '#f46f65',
                background: '#fff url("./img/p0080_m.jpg") center'
            }).then(result => {
                if (result.value) {
                    location.reload();
                }
            })

        } else {
            alert(xhr.status);
        }
    }

    var url = `./php/mem/membyeshop.php?mem_NO=${iamamiwhami}&order_no=${e}`;
    xhr.open("Get", url, true);
    xhr.send(null);

}
//撈訂單
function getmemshopdata(iamamiwhami) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        if (xhr.status == 200) {
            showmmemshopdata(xhr.responseText, iamamiwhami);
        } else {
            alert(xhr.status);
        }
    }

    var url = "./php/mem/memshop.php?mem_NO=" + iamamiwhami;
    xhr.open("Get", url, true);
    xhr.send(null);
}

//收藏跳轉去巨知識/////////////////////////////////////////
function gotoknowledge(e) {
    //  console.log(e);
    location.href = "http://140.115.236.71/demo-projects/dd104/dd104g4/knowledge.html?" + "BOOK" + e;
}
//////////////////////////////////////////////////////////////////////////////////
window.addEventListener("load", doFirst);
