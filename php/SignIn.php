<?php
$memEmail = $_POST["mem_Email"];
$memPsw = $_POST["mem_Psw"];

try {
    require_once "connectBook.php";
    if ($memPsw && $memEmail != "") {

        $sqlLogin = "select * from `member` where mem_Email=:mem_Email and mem_Psw=:mem_Psw;";
        $login = $pdo->prepare($sqlLogin);
        $login->bindValue(":mem_Email", $memEmail);
        $login->bindValue(":mem_Psw", $memPsw);
        $login->execute();

        if ($login->rowCount() != 0) {
            $dataRow = $login->fetch(PDO::FETCH_ASSOC);

            if (($memEmail == $dataRow['mem_Email']) && ($memPsw == $dataRow['mem_Psw']) && ($dataRow['mem_State'] == 1)) {

                session_start();
                $_SESSION["mem_NO"] = $dataRow["mem_NO"];
                $_SESSION["mem_Name"] = $dataRow["mem_Name"];
                $_SESSION["mem_Email"] = $dataRow["mem_Email"];
                $_SESSION["mem_Psw"] = $dataRow["mem_Psw"];
                echo "登入成功";
            } else if (($memEmail == $dataRow['mem_Email']) && ($memPsw == $dataRow['mem_Psw']) && ($dataRow['mem_State'] == 0)) {
                echo "帳號停權";
            } else {
                echo "帳號密碼錯誤";
            }

        }

    }
} catch (PDOException $msg) {
    echo "例外行號 : ", $msg->getLine(), "<br>";
    echo "例外原因 : ", $msg->getMessage(), "<br>";
}
