<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");


$sqlCheck = 'select activity_NO, mem_NO from `registration` where activity_NO = :activityNum and mem_NO = :mem_NO';
$regiterCheck = $pdo->prepare($sqlCheck);

$regiterCheck->bindValue(':activityNum', $_POST['activityNum']);
$regiterCheck->bindValue(':mem_NO', $_POST['mem_NO']);
$regiterCheck->execute();

if ($regiterCheck->rowCount() != 0) {
    echo "already";
} else {

    $sql = 'INSERT INTO `registration` VALUES (NULL, :activityNum, :mem_NO, :signUpDate, "", 0, 1)';
    $regitration = $pdo->prepare($sql);
    $regitration->bindValue(":activityNum", $_POST['activityNum']);
    $regitration->bindValue(":signUpDate", date("Y-m-d"));
    $regitration->bindValue(":mem_NO", $_POST['mem_NO']);
    $regitration->execute();


    $signUp_NO = $pdo->lastInsertId();
    //將檔案格式的資訊拿掉
    $myCanvasURL = str_replace('data:image/png;base64,', '', $_POST['myCanvasURL']);
    $data = base64_decode($myCanvasURL);
    $fileName = $signUp_NO . "_" . $_POST['uploadSignUpImg'];
    // 放進資料夾
    $file = "regi_images/" . $fileName;
    $success = file_put_contents($file, $data);

    if ($success) {
        $sqll = "update `registration` set registration_Pic = :fileName where registration_NO = {$signUp_NO}";
        $updateRegisterimg = $pdo->prepare($sqll);
        $updateRegisterimg->bindValue(":fileName", $file);
        $updateRegisterimg->execute();
        echo "complete";
    } else {
        echo 'error';
    }
}
