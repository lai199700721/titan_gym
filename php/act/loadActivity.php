<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");



if ($_POST["getActNum"] == 0) {
    // 取得可報名活動及當期可投票活動
    $date = date("Ymd");
    $sql = "select a.activity_NO, a.activity_Name, a.activity_Date, a.activity_Info, a.activity_Pic, a.maximum_num, a.activity_Poster, count(r.activity_NO) as 'signUp_NO' from `activity` a left join (select * from `registration` where `registration_State` = 1) r on (a.activity_NO = r.activity_NO) where (a.activity_State = 1 or a.activity_State = 3) and a.activity_NO >= :actNum group by a.activity_NO";

    $Allactivity = $pdo->prepare($sql);
    $Allactivity->bindValue(":actNum", $_POST["actNum"]);
    $Allactivity->execute();

    while ($i = $Allactivity->fetchObject()) {
        $actData[] = array(
            "activity_NO" => $i->activity_NO,
            "activity_Name" => $i->activity_Name,
            "activity_Date" => $i->activity_Date,
            "activity_Info" => $i->activity_Info,
            "activity_Pic" => $i->activity_Pic,
            "maximum_num" => $i->maximum_num,
            "signUp_NO" => $i->signUp_NO,
            "activity_Poster" => $i->activity_Poster,
        );
    };


    echo json_encode($actData);
} else {

    // 取得當期活動編號
    $sql = "select activity_NO from `activity` where activity_State = 3";
    $curActNum = $pdo->query($sql);

    echo json_encode(["actNum" => $curActNum->fetchObject()->activity_NO]);
}
