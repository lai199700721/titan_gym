<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");


if ($_POST["isChange"] == 0) {
    // $sql = "select * from `registration` order by activity_NO";
    $sql = "select a.activity_NO, a.activity_State, r.mem_NO, r.votes, r.registration_Date, r.registration_Pic, r.registration_State from `activity` a right join `registration` r on (a.activity_NO = r.activity_NO) order by a.activity_NO, r.votes desc";
    $registration = $pdo->query($sql);

    while ($Row = $registration->fetchObject()) {

        $data[] = ["activity_NO" => $Row->activity_NO, "mem_NO" => $Row->mem_NO, "registration_Pic" => $Row->registration_Pic, "registration_Date" => $Row->registration_Date, "registration_State" => $Row->registration_State, "activity_State" => $Row->activity_State];
    }

    echo json_encode($data);
} else {
    $sql = "update `registration` set registration_State = :registration_State where mem_NO = :mem_NO";
    $registration = $pdo->prepare($sql);
    $registration->bindValue(":registration_State", $_POST["registration_State"]);
    $registration->bindValue(":mem_NO", $_POST["mem_NO"]);
    $registration->execute();

    echo json_encode(["status" => "ok"]);
}
