<?php
// require_once('connectBooksACT.php');
require_once("../connectBook.php");


$sql = "select activity_NO, mem_NO from vote where activity_NO = :act_Num and mem_NO = :mem_NO";
$vote = $pdo->prepare($sql);


$jsonObj_vote = json_decode(file_get_contents('php://input'), true);

$vote->bindValue(":act_Num", $jsonObj_vote["act_Num"]);
$vote->bindValue(":mem_NO", $jsonObj_vote["mem_NO"]);
$vote->execute();

if ($vote->rowCount() !== 0) {
    echo "voted";
} else {
    $sql = 'INSERT INTO `vote` VALUES (NULL, :act_Num, :mem_NO, :register_no, :date)';
    $voting = $pdo->prepare($sql);
    $voting->bindValue(":act_Num", $jsonObj_vote["act_Num"]);
    $voting->bindValue(":mem_NO", $jsonObj_vote["mem_NO"]);
    $voting->bindValue(":register_no", $jsonObj_vote["register_no"]);
    $voting->bindValue(":date", date("Ymd"));
    $voting->execute();
    echo "down";

    $sql = "update `registration` set votes = votes + 1 where registration_NO = {$jsonObj_vote["register_no"]}";
    $pdo->exec($sql);
}
