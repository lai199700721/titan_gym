<?php
// require_once("connectBooksACT.php");
require_once("../connectBook.php");

$actNum = json_decode(file_get_contents("php://input"))->actNum;


$sql = "select registration_NO from `registration` where activity_NO = :actNum and registration_State = 1";
$howManyCompetitors = $pdo->prepare($sql);
$howManyCompetitors->bindValue(":actNum", $actNum);
$howManyCompetitors->execute();


while ($Row = $howManyCompetitors->fetchObject()) {
    $allCompetitor[] = $Row->registration_NO;
};



$sql = "select r.registration_NO, c.comment_NO, c.comment_Date, c.comment_content, m.mem_Pic from `registration` r left join `comment` c on (r.registration_NO = c.registration_NO) join `member` m on (c.mem_NO = m.mem_NO) where r.activity_NO = :actNum and r.registration_State = 1 and (c.comment_state = 1 or c.comment_state = 2) order by r.registration_NO, c.comment_Date";


$comment = $pdo->prepare($sql);
$comment->bindValue(":actNum", $actNum);
$comment->execute();


$requireData = [];
foreach ($allCompetitor as $index => $registration_NO) {
    $requireData[] = [];
}

while ($Row = $comment->fetchObject()) {
    $requireData[array_search($Row->registration_NO, $allCompetitor)][] =
        [$Row->comment_NO, $Row->comment_content, $Row->mem_Pic];
};





echo json_encode($requireData);
