<?php
  try{
    
    //抓到使用者最新的歷史訓練菜單編號
    require_once("../connectBook.php");
    $sql = "select `historyMenu_No` from `history_menu` where `mem_NO`= ? order by `historyMenu_Date` DESC limit 1 ";
    $lastMenu = $pdo->prepare($sql);
    $lastMenu->bindValue(1, $_GET["mem_NO"]);
    $lastMenu->execute();
    $lastMenuRow = $lastMenu->fetch(PDO::FETCH_ASSOC);  
    // echo print_r(  $lastMenuRow);
      
    //去找歷史訓練菜單編號所對應的運動項目
    $sql2 = "select *  from `menu` m join `sport_equipment` s on m.sport_No = s.sport_No where m.historyMenu_No = ? and s.State = 1;";
    $showlastMenu = $pdo->prepare($sql2);
    $showlastMenu->bindValue(1, $lastMenuRow["historyMenu_No"] );
    $showlastMenu->execute();

    //如果找不到
    if( $showlastMenu->rowCount() == 0 ){ 
      //找不到,表示新來的
      echo "{}";
    }else{ 
      //找得到歷史訓練菜單編號
      $showlastMenuRow = $showlastMenu->fetchAll(PDO::FETCH_ASSOC);

      //用陣列檢查是否有success(1)
      $arr = array();
      foreach($showlastMenuRow as $value ){
        array_push($arr,$value["success"]) ;
      }
           
      if(in_array(1,$arr)){
        // "這是訓練菜單";
        // echo $showlastMenuRow[0]["historyMenu_No"];
        echo "{}";
      }else{
        //"這是規劃菜單"; 回傳規劃的內容
        echo json_encode($showlastMenuRow);
      }
    }     
  }catch(PDOException $e){
      $errMsg .= "錯誤原因 : ".$e -> getMessage(). "<br>";
      $errMsg .= "錯誤行號 : ".$e -> getLine(). "<br>";
  }
?>
