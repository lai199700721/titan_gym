<?php

$errMsg = "";
try{
     require_once("../connectBook.php");
    //按保存的都表示未完成訓練,預設給0
    $sql = "insert into `menu`(`historyMenu_No`, `sport_No`, `group_Num`, `times_Num`, `kg`,`success`) VALUES (?,?,?,?,?,?) ";
    $menu = $pdo->prepare($sql);
    $menu->bindValue(1, $_GET["historyMenu_No"]);
    $menu->bindValue(2, $_GET["sport_No"]);
    $menu->bindValue(3, $_GET["group_Num"]);
    $menu->bindValue(4, $_GET["times_Num"]);
    $menu->bindValue(5, $_GET["kg"]);
    $menu->bindValue(6, 0);
    $menu->execute();  
}catch(PDOException $e){
    $errMsg .= "錯誤原因 : ".$e -> getMessage(). "<br>";
    $errMsg .= "錯誤行號 : ".$e -> getLine(). "<br>";
  }
?>
