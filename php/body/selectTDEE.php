<?php
    $errMsg = "";
    try{
        require_once("../connectBook.php");
        $sql ="select `body_TDEE` from `body`where `mem_NO` =?order by body_NO DESC";
        $selectTDEE = $pdo->prepare($sql);
        $selectTDEE->bindValue(1,$_GET["mem_NO"]);
        $selectTDEE->execute();

        $selectTDEERow = $selectTDEE->fetch();
        echo json_encode($selectTDEERow);

    }catch(PDOException $e){
        $errMsg .= "錯誤原因 : ".$e -> getMessage(). "<br>";
        $errMsg .= "錯誤行號 : ".$e -> getLine(). "<br>";

    
    }
?>
