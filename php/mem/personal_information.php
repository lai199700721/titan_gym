<?php
try {

    require_once "../connectBook.php";
    session_start();
    $mem_No = $_SESSION["mem_NO"];
    // $mem_No = 48;

    $personalData = "select m.mem_Name,m.mem_Nick_Name,m.mem_Email,m.mem_Psw ,m.mem_Pic,b.body_Cm,b.body_Kg,b.body_TDEE,b.body_Date
    from `member` m  LEFT OUTER join  body b on m.mem_NO=b.mem_NO
    WHERE m.mem_NO=:mem_NO order by b.body_NO DESC;
    ;";

    $data = $pdo->prepare($personalData);
    $data->bindValue(":mem_NO", $mem_No);
    $data->execute();

    $dataRow = $data->fetch(PDO::FETCH_ASSOC);

    echo json_encode($dataRow);

} catch (PDOException $msg) {
    echo "例外行號 : ", $msg->getLine(), "<br>";
    echo "例外原因 : ", $msg->getMessage(), "<br>";
}
