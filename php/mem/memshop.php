<?php
try{

require_once("../connectBook.php");

$sql = "SELECT `order_time`,`order_amount`,`shipping_status`,`order_no` FROM `order_master` WHERE mem_NO = :mem_NO";
$memshop = $pdo->prepare($sql);
$memshop->bindValue(":mem_NO", $_GET['mem_NO']);
$memshop->execute();
$memshopRow = $memshop->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($memshopRow);

}catch(PDOException $e){
    echo $e->getMessage();
}
?>
