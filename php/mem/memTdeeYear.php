<?php
try{
  require_once("../connectBook.php");

  // $sql = "select DISTINCT Year(body_Date) from `body` where mem_NO=:mem_NO order by body_Date desc";
  $sql = "select Year(body_Date) as 'year', Month(body_Date) as 'month', body_TDEE from `body` where mem_NO=:mem_NO order by body_Date";
  // select DISTINCT Year(body_Date) as 'year', GROUP_CONCAT(Month(body_Date)) as 'month', GROUP_CONCAT(body_TDEE) as 'TDEE' from `body` where mem_NO=2 GROUP by year order by year desc
    
    $member = $pdo->prepare($sql);
    $member->bindValue(":mem_NO",$_GET["mem_NO"]);
    //$_GET["mem_NO"]
    $member->execute();

    if( $member->rowCount() == 0 ){ //找不到
        //傳回空的JSON字串
        $arr_year=0;
        $arr=0;
        echo json_encode(array("year" => $arr_year, "data" => $arr));
      }else{ //找得到
       $memRow = $member->fetchAll(PDO::FETCH_ASSOC);
       $arr = array();
       $arr_year = array();
        $x = 0;  
       
       foreach($memRow as $key => $val){
         $year = $val['year'];
         $month = $val['month'];
         $body_TDEE = $val['body_TDEE'];
       
         if(array_search($year,$arr_year) == false){
          $x++;
          $arr_year[$x] = $year;
         }
         

         $arr[$year][$month] = $body_TDEE;
         ksort($arr[$year]);
         ksort($arr);
       }
      
       echo json_encode(array("year" => $arr_year, "data" => $arr));
      }	
   
}catch(PDOException $e){
  echo $e->getMessage();
}
?>
