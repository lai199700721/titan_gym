<?php
try {
    require_once("../connectBook.php");
    //執行度
    $sql = "select success from history_menu join menu on history_menu.historyMenu_No = menu.historyMenu_No where mem_NO = :mem_NO AND historyMenu_Date > DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH)";
    $mempowerC = $pdo->prepare($sql);
    $mempowerC->bindValue(":mem_NO", $_GET['mem_NO']);
    $mempowerC->execute();
    if ($mempowerC->rowCount() == 0) { //找不到
        //傳回空的JSON字串
        $ttP = 0;
    } else { //找得到
        $mempowerCRow = $mempowerC->fetchAll(PDO::FETCH_ASSOC);
        $arrC = array();
        $ttP;
        $z = 0;
        foreach ($mempowerCRow as $key => $val) {

            $arrC[$key] = $val['success'];
            if ($val['success'] == 1) {
                $z++;
            }
        }
        //執行度的結果
        $ttP = round($z / (count($arrC)) * 100);
    }

/////////////////////////////////////////////////////////////////
    //舉辦活動的比例
    $sql = "select mem_NO from `activity` where activity_Date > DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH)";
    $mempowerA = $pdo->query($sql);
    if ($mempowerA->rowCount() == 0) { //找不到
        //傳回空的JSON字串
        $actA = 0;

    } else { //找得到
        $mempowerARow = $mempowerA->fetchAll(PDO::FETCH_ASSOC);
        $arr = array();
        $actA;
        $x = 0;
        foreach ($mempowerARow as $key => $val) {
            $arr[$key] = $val['mem_NO'];
            if ($val['mem_NO'] == $_GET['mem_NO']) {
                $x++;
            }
        }
        $actA = round($x / (count($arr)) * 100);
    }
//參賽活動的比例
    $sql = "select mem_NO from `registration` WHERE registration_Date > DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH)";
    $mempowerB = $pdo->query($sql);
    if ($mempowerB->rowCount() == 0) { //找不到
        //傳回空的JSON字串
        $actC = 0;
        $activitymempower = 0;

    } else { //找得到
        $mempowerBRow = $mempowerB->fetchAll(PDO::FETCH_ASSOC);
        $arrB = array();
        $actC;
        $y = 0;
        foreach ($mempowerBRow as $key => $val) {
            $arrB[$key] = $val['mem_NO'];
            if ($val['mem_NO'] == $_GET['mem_NO']) {
                $y++;
            }
        }
        $actC = round($y / (count($arrB)) * 100);
        //活耀度的結果
        $activitymempower = round(($actA + $actC) / 2);
    }
    //////////////////////////////////////////////////////////////
    //耐力度
    $sql = "select mem_NO from `history_menu` where mem_NO = :mem_NO and historyMenu_Date >DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH)";
    $mempowerD = $pdo->prepare($sql);
    $mempowerD->bindValue(":mem_NO", $_GET['mem_NO']);
    $mempowerD->execute();
    if ($mempowerD->rowCount() == 0) { //找不到
        //傳回空的JSON字串
        $nicelongerpower = 0;

    } else { //找得到
        $mempowerDRow = $mempowerD->fetchAll(PDO::FETCH_ASSOC);
        $nicelongerpower = round((count($mempowerDRow) / 20) * 100);
    }
    /////////////////////////////////////////////////////////////////
    //討論力
    $sql = "select mem_NO from `comment` where  comment_Date >DATE_SUB(CURRENT_DATE(),INTERVAL 1 MONTH)";
    $mempowerE = $pdo->query($sql);
    if ($mempowerE->rowCount() == 0) { //找不到
        //傳回空的JSON字串
        $commentE = 0;

    } else { //找得到
        $mempowerERow = $mempowerE->fetchAll(PDO::FETCH_ASSOC);
        $arrE = array();
        $commentE;
        $w = 0;
        foreach ($mempowerERow as $key => $val) {
            $arrE[$key] = $val['mem_NO'];
            if ($val['mem_NO'] == $_GET['mem_NO']) {
                $w++;
            }
        }
        $commentE = round(($w / count($arrE)) * 100);
    }
    $luck = round(($ttP + $activitymempower + $nicelongerpower + $commentE + 80) / 5);
///////////////////////////////////////////////////////////////////
    echo json_encode(array($ttP, $activitymempower, $nicelongerpower, $commentE, $luck));

} catch (PDOException $e) {
    echo $e->getMessage();
}
