<?php
try{
 require_once("../connectBook.php");

  $sql = "
  select Year(historyMenu_Date) as 'year',Month(historyMenu_Date) as 'month',Day(historyMenu_Date) as 'day',sport_name,group_Num,times_Num,kg 
  FROM history_menu JOIN menu ON history_menu.historyMenu_No = menu.historyMenu_No 
  JOIN sport_equipment on menu.sport_No = sport_equipment.sport_No 
  WHERE mem_NO=:mem_NO AND success=1
  order by historyMenu_Date DESC;";
  
    $memhistory = $pdo->prepare($sql);
    $memhistory->bindValue(":mem_NO", $_GET["mem_NO"]);
    $memhistory->execute();

    if( $memhistory->rowCount() == 0 ){ //找不到
        //傳回空的JSON字串
        echo "{}";
      }else{ //找得到
       $memhistoryRow = $memhistory->fetchAll(PDO::FETCH_ASSOC);
       $arrA = array();
       $arrB = array();
       $arrC = array();
       $arrD = array();
       $arr_year = array();
       $arr_month = array();
       $arr_day = array();

        $x = 0;  
       
       foreach($memhistoryRow as $key => $val){
         $year = $val['year'];
         $month = $val['month'];
         $day = $val['day'];
         $sport_name = $val['sport_name'];
         $group_Num = $val['group_Num'];
         $times_Num = $val['times_Num'];
         $kg = $val['kg'];

        if(array_search($year,$arr_year) == false){
          $x++;
          $arr_year[$x] = $year;
         }
         if(array_search($month,$arr_month) == false){
          $x++;
          $arr_month[$year][$x]=$month;
          $arr_month[$x] = $month;   
         }
         if(array_search($day,$arr_day) == false){
          $x++;
          $arr_day[$year][$month][$x]=$day;
          $arr_day[$x] = $day;   
         }
         
         $arrA[$year][$month][$day][$key]= $sport_name;
         $arrB[$year][$month][$day][$key][$sport_name]= $group_Num ;
         $arrC[$year][$month][$day][$key][$sport_name]=$times_Num;
         $arrD[$year][$month][$day][$key][$sport_name]= $kg;
       }
        
echo json_encode(array("year" => $arr_year,"month" => $arr_month,"day" => $arr_day, "sportnamedata" => $arrA ,"groupNumdata" =>$arrB,"timesNumdata" => $arrC,"kgdata"=>$arrD));
      }	
   
}catch(PDOException $e){
  echo $e->getMessage();
}
?>
