<?php
try {
    require_once "connectBook.php";

    //抓前面欄位的值
    $checkEmail = "select * from `member` where mem_Email='{$_POST["mem_Email"]}';";

    $memEmail = $pdo->query($checkEmail);

    if ($memEmail->rowCount() == 0) {

        //找不到帳號時
        $addmember = "INSERT INTO `member`(mem_Name,mem_Nick_Name,mem_Email,mem_Psw)
                     VALUES(:mem_Name,:mem_Nick_Name,:mem_Email,:mem_Psw)";
        $member = $pdo->prepare($addmember);

        $member->bindValue(":mem_Name", $_POST["mem_Name"]);
        $member->bindValue(":mem_Nick_Name", $_POST["mem_Nick_Name"]);
        $member->bindValue(":mem_Email", $_POST["mem_Email"]);
        $member->bindValue(":mem_Psw", $_POST["mem_Psw"]);
        $member->execute();

        if ($member->rowCount() != 0) {
            $sqlLogin = "select * from `member` where mem_Email=:mem_Email and mem_Psw=:mem_Psw;";
            $member = $pdo->prepare($sqlLogin);
            $member->bindValue(":mem_Email", $_POST["mem_Email"]);
            $member->bindValue(":mem_Psw", $_POST["mem_Psw"]);
            $member->execute();

            $dataRow = $member->fetch(PDO::FETCH_ASSOC);

            session_start();
            $_SESSION["mem_NO"] = $dataRow["mem_NO"];
            $_SESSION["mem_Name"] = $dataRow["mem_Name"];
            $_SESSION["mem_Email"] = $dataRow["mem_Email"];
            $_SESSION["mem_Psw"] = $dataRow["mem_Psw"];
            echo "註冊成功";

        } else {
            echo "註冊失敗";

        }

    }
} catch (PDOException $msg) {
    echo "例外行號 : ", $msg->getLine(), "<br>";
    echo "例外原因 : ", $msg->getMessage(), "<br>";
}
