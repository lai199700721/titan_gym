<?php
$adminId = $_POST["admin_Id"];
$adminPsw = $_POST["admin_Psw"];

try {
    require_once "connectBook.php";
    if ($adminId && $adminPsw != "") {
        $sqlLogin = "select  *  from  `administrator` where admin_Id=:admin_Id and admin_Psw=:admin_Psw;";

        $adminlogin = $pdo->prepare($sqlLogin);
        $adminlogin->bindValue(":admin_Id", $adminId);
        $adminlogin->bindValue(":admin_Psw", $adminPsw);
        $adminlogin->execute();

        if ($adminlogin->rowCount() != 0) {
            $dataRow = $adminlogin->fetch(PDO::FETCH_ASSOC);

            if (($adminId == $dataRow['admin_Id']) && ($adminPsw == $dataRow['admin_Psw'])) {

                session_start();
                $_SESSION["admin_Name"] = $dataRow["admin_Name"];
                $_SESSION["admin_Id"] = $dataRow["admin_Id"];
                $_SESSION["admin_Psw"] = $dataRow["admin_Psw"];
                echo "登入成功";
            } else {
                echo "登入失敗";
            }

        }

    }
} catch (PDOException $msg) {
    echo "例外行號 : ", $msg->getLine(), "<br>";
    echo "例外原因 : ", $msg->getMessage(), "<br>";
}
